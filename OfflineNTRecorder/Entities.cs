﻿using System;
using System.Collections.Generic;

namespace OfflineNTRecorder
{
    public class JobDetails
    {
        public string MainFolder { get; }
        public string SettingsFile { get; }
        public string TestFilesFolder { get; }
        public string TestProfile { get; set; }

        public bool Mobile { get; set; }
        public bool AddProfile { get; set; }
        public bool EditProfile { get; set; }
        public bool AddSurvey { get; set; }
        public bool Error { get; set; }

        public string Url { get; set; }
        public string Scenario { get; set; }
        public List<string> Tasks { get; set; }
        public List<string> SurveyQuestions { get; set; }
        public List<string> SurveyAnswers { get; set; }

        public string TesterName { get; set; }
        public string TesterFolder { get; set; }
        public string MobileTestFile { get; set; }
        public string SurveyAnswerFile { get; set; }

        public JobDetails()
        {
            MainFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\OfflineNTRecorder\";
            SettingsFile = MainFolder + @"Logs\Settings.txt";
            TestFilesFolder = MainFolder + @"Test Files\";
            TestProfile = MainFolder + "Default Profile.txt";
            Error = false;
        }
    }
}
