﻿using System;
using System.Windows;
using System.Windows.Input;

namespace OfflineNTRecorder
{
	/// <summary>
	/// Interaction logic for MsgBox.xaml
	/// </summary>
	public partial class MsgBox : Window
    {
        private bool result = false;
        private bool replay = false;

        public bool Result { get { return result; } }
        public bool Replay { get { return replay; } }

        public enum MsgBoxType
        {
            Ok,
            YesNo,
            MicTest
        }

		public MsgBox(String Title, String Message, MsgBoxType Type)
		{
			this.InitializeComponent();

            switch (Type)
            {
                case MsgBoxType.Ok:
                    ButtonNo.Visibility = Visibility.Hidden;
                    ButtonYes.Visibility = Visibility.Hidden;
                    ButtonOk.Focus();
                    break;
                case MsgBoxType.YesNo:
                    ButtonOk.Visibility = Visibility.Hidden;
                    ButtonYes.Focus();
                    break;
                case MsgBoxType.MicTest:
                    ButtonOk.Content = "Replay";
                    break;
            }
            this.MsgBoxTitle.Text = Title;
            this.MsgBoxInfo.Text = Message;
			// Insert code required on object creation below this point.
		}

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            replay = true;
            this.Close();
        }

        private void ButtonNo_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonYes_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            result = true;
        }

        private void MsgBoxTitle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
	}
}