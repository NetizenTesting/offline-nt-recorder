﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.IO.Compression;
using System.ComponentModel;
using System.Diagnostics;
using System.Timers;
using System.IO;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Interaction logic for InstallControl.xaml
    /// </summary>
    public partial class InstallControl : UserControl
    {
        string FileLocation;
        string Folder;
        delegate void UpdateUI();
        delegate void UpdateUIInt(int value);

        public InstallControl(string FileLocation, string Folder)
        {
            InitializeComponent();
            this.FileLocation = FileLocation;
            this.Folder = Folder;
        }
        private void Storyboard_Completed(object sender, EventArgs e)
        {
            WebClient client = new WebClient();
            client.DownloadProgressChanged += client_DownloadProgressChanged;
            client.DownloadFileCompleted += client_DownloadFileCompleted;
            client.DownloadFileAsync(new Uri("http://app.netizentesting.com/Encoder.zip"), FileLocation);
        }

        void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Cancelled == false && e.Error == null)
            {
                this.Dispatcher.BeginInvoke(new UpdateUI(DownloadComplete));
            }
            else
            {
                this.Dispatcher.BeginInvoke(new UpdateUI(DownloadError));
            }
        }

        BackgroundWorker extractWorker = new BackgroundWorker();

        void DownloadComplete()
        {
            this.ProgressBar.IsIndeterminate = true;
            this.ProgressBar.Value = 100;
            this.ProgressBar.Minimum = 0;
            this.ProgressBar.Maximum = 100;
            FileInfo f = new FileInfo(FileLocation);
            if (f.Length != 26901203)
            {
                MsgBox msgBox = new MsgBox("Error", "Error occur downloading Expression Encoder", MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();
                Window.GetWindow(this).Close();
                return;
            }
            Info.Text = "Extracting Expression Encoder";
            ProgressBlock.Text = "";
            extractWorker.DoWork += extractWorker_DoWork;
            extractWorker.RunWorkerAsync();
            loadingTimer.Elapsed += loadingTimer_Elapsed;
            extractWorker.RunWorkerCompleted += extractWorker_RunWorkerCompleted;
            loadingTimer.Start();
        }

        void extractWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loadingTimer.Stop();
            bool info = (bool) e.Result;
            if (info)
            {
                this.Dispatcher.BeginInvoke(new UpdateUI(restart));
            }
            else
            {
                this.Dispatcher.BeginInvoke(new UpdateUI(InstallError));
            }
        }

        void loadingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUI(LoadingBarUpdate));
        }

        bool UpDown = false;

        void LoadingBarUpdate()
        {
            if (ProgressBar.Value >= 100)
            {
                UpDown = true;
            }
            if (ProgressBar.Value <= 0)
            {
                UpDown = false;
            }
            if (UpDown)
            {
                ProgressBar.Value -= 1;
            }
            else
            {
                ProgressBar.Value += 1;
            }
        }

        Timer loadingTimer = new Timer(10);
        void extractWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ZipFile.ExtractToDirectory(FileLocation, Folder);

                //using (ZipFile zip1 = ZipFile.Read(FileLocation))
                //{
                //    // here, we extract every entry, but we could extract conditionally
                //    // based on entry name, size, date, checkbox status, etc.  
                //    foreach (ZipEntry zip in zip1)
                //    {
                //        zip.Extract(Folder, ExtractExistingFileAction.OverwriteSilently);
                //    }
                //}
                this.Dispatcher.BeginInvoke(new UpdateUI(UpdateInstalling));
                Process p = new Process();
                ProcessStartInfo info = new ProcessStartInfo(Folder + "encoder\\setup.exe", "-q");
                p.StartInfo = info;
                p.Start();
                p.WaitForExit();
                e.Result = true;
            }
            catch (Exception)
            {
                this.Dispatcher.BeginInvoke(new UpdateUI(InstallError));
                e.Result = false;
            }
        }

        void restart()
        {
            MsgBox msgBox = new MsgBox("Install Complete", "Installation complete. Please restart your PC.", MsgBox.MsgBoxType.Ok);
            msgBox.ShowDialog();
            Window.GetWindow(this).Close();
        }

        void UpdateInstalling()
        {
            Info.Text = "Installing. Process will take up to 10 minutes.";
        }

        void DownloadError()
        {
            MsgBox msgBox = new MsgBox("Error", "Error occur downloading Expression Encoder", MsgBox.MsgBoxType.Ok);
            msgBox.ShowDialog();
            Window.GetWindow(this).Close();
        }

        void InstallError()
        {
            MsgBox msgBox = new MsgBox("Error", "Error occur installing Expression Encoder", MsgBox.MsgBoxType.Ok);
            msgBox.ShowDialog();
            Window.GetWindow(this).Close();
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUIInt(UpdatePercentage), e.ProgressPercentage);
        }

        void UpdatePercentage(int value)
        {
            ProgressBlock.Text = value.ToString() + "%";
            ProgressBar.Value = value;
        }

        private void EncoderControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }
    }
}
