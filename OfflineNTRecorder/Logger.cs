﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfflineNTRecorder
{
    class ErrorLogger
    {
        static string LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\OfflineNTRecorder\Logs\";
        string file;
        string control;

        public ErrorLogger(string Filename)
        {
            control = "";
            CreateDirectoryIfNotExist(LogFileDirectory);
            file = Filename;
            DeleteFileIfExists(LogFileDirectory + file);
        }

        public ErrorLogger(string Filename, bool append, string Control)
        {
            CreateDirectoryIfNotExist(LogFileDirectory);
            file = Filename;
            control = Control;

            if (!append)
            {
                DeleteFileIfExists(LogFileDirectory + file);
            }
        }

        void CreateDirectoryIfNotExist(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        void DeleteFileIfExists(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        } 

        public void AppendLog(string Log)
        {
            Log = DateTime.UtcNow.AddHours(8).ToString() + "[GMT+8]: " + control + Log;
            using (StreamWriter logFile = new StreamWriter(LogFileDirectory + file, true))
            {
                logFile.WriteLine(Log);
            }
        }

        /// <summary>
        /// Prints full exception message and stack trace to ErrorLogger
        /// </summary>
        public void ExceptionLog(Exception exception)
        {
            using (StreamWriter logFile = new StreamWriter(LogFileDirectory + file, true))
            {
                logFile.Write(DateTime.UtcNow.AddHours(8).ToString() + "[GMT+8]: " + control);
                logFile.Write("Exception Occured: ");

                do
                {
                    logFile.WriteLine(exception.GetType().ToString());
                    logFile.WriteLine("Message = " + exception.Message);
                    logFile.WriteLine("Stack Trace:");
                    logFile.WriteLine(exception.StackTrace);
                    logFile.Write("Inner Exception:");
                    exception = exception.InnerException;
                } while (exception != null);

                logFile.WriteLine();
            }
        }
    }
}