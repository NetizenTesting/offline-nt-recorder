﻿using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.ScreenCapture;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace OfflineNTRecorder
{

    /// <summary>
    /// Interaction logic for ScreenCapture.xaml
    /// </summary>
    public partial class ScreenCapture : UserControl
    {
        public delegate void ScreenCaptureCompletedHandler(object sender, EventArgs e);
        public event ScreenCaptureCompletedHandler OnScreenCaptureComplete;
        delegate void UpdateUI();
        private enum RecordingState
        {
            Scenario,
            Recording,
            Paused,
            Completed
        }
        private string scenario, fileLocation;
        private EncoderDevice audioDevice;
        private List<string> instructions;
        private RecordingState currentState = RecordingState.Scenario;
        private int currentInstruction = 0;
        private ScreenCaptureJob screenCapture = new ScreenCaptureJob();
        BackgroundWorker jobSetup = new BackgroundWorker();
        Timer timer = new Timer(100);
        public List<long> TaskCompletionTime = new List<long>();
        //MouseClickCaptureForm mouseForm = new MouseClickCaptureForm();

        private void SetRichTextBox(string text, bool type)
        {
            if (type)
            {
                RichTextBoxInfo.Document.Blocks.Clear();
                RichTextBoxInfo.Document.Blocks.Add(new Paragraph(new Run("Task " + (currentInstruction + 1).ToString() + ": " + text)));
                RichTextBoxInfo.CaretPosition = RichTextBoxInfo.CaretPosition.DocumentStart;
                RichTextBoxInfo.ScrollToHome();
            }
            else
            {
                RichTextBoxInfo.Document.Blocks.Clear();
                RichTextBoxInfo.Document.Blocks.Add(new Paragraph(new Run("Scenario: " + text)));
                RichTextBoxInfo.CaretPosition = RichTextBoxInfo.CaretPosition.DocumentStart;
                RichTextBoxInfo.ScrollToHome();
            }
        }

        public ScreenCapture(string Scenario, List<string> Instructions, string FileLocation, EncoderDevice AudioDevice, string Url)
        {
            this.InitializeComponent();
            RichTextBoxInfo.IsReadOnly = true;
            RichTextBoxInfo.IsReadOnlyCaretVisible = false;
            ButtonBack.IsEnabled = false;
            ButtonNext.IsEnabled = false;
            this.scenario = Scenario;
            this.instructions = Instructions;
            this.audioDevice = AudioDevice;
            this.fileLocation = FileLocation;
            SetRichTextBox(scenario, false);
            jobSetup.DoWork += jobSetup_DoWork;
            jobSetup.RunWorkerCompleted += jobSetup_RunWorkerCompleted;
            timer.Elapsed += timer_Elapsed;
            Process.Start(Url);
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUI(TimeUpdate), null);
        }

        void TimeUpdate()
        {
            TimeSpan ts = screenCapture.Statistics.Duration;
            if (ts.Hours == 0)
            {
                labelTimer.Text = ts.Minutes.ToString("D2") + ":" + ts.Seconds.ToString("D2");
            }
            else
            {
                labelTimer.Text = ts.Hours.ToString("D2") + ":" + ts.Minutes.ToString("D2") + ":" + ts.Seconds.ToString("D2");
            }
        }

        void jobSetup_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            screenCapture.Start();
            //mouseForm.Show();
            //mouseForm.Capture = true;
            timer.Start();
            currentState = RecordingState.Recording;
            ButtonNext.IsEnabled = true;
            ButtonStart.Content = "Pause";
            SetRichTextBox(instructions[currentInstruction], true);
        }

        void jobSetup_DoWork(object sender, DoWorkEventArgs e)
        {
            screenCapture.ShowCountdown = true;
            screenCapture.CaptureFollowCursor = false;
            screenCapture.OutputScreenCaptureFileName = fileLocation;
            screenCapture.CaptureMouseCursor = true;
            screenCapture.AddAudioDeviceSource(audioDevice);
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            MsgBox msgBox = new MsgBox("Exit Confirmation", "Are you sure you want to quit the application?", MsgBox.MsgBoxType.YesNo);
            msgBox.ShowDialog();
            if (msgBox.Result)
            {
                Window.GetWindow(this).Close();
            }
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            switch (currentState)
            {
                case RecordingState.Scenario:
                    jobSetup.RunWorkerAsync();
                    break;
                case RecordingState.Recording:
                    screenCapture.Pause();
                    ButtonStart.Content = "Start";
                    //mouseForm.Capture = false;
                    currentState = RecordingState.Paused;
                    break;
                case RecordingState.Paused:
                    screenCapture.Resume();
                    ButtonStart.Content = "Pause";
                    //mouseForm.Capture = true;
                    currentState = RecordingState.Recording;
                    break;
            }
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            currentInstruction--;
            ButtonNext.Background = Brushes.White;
            ButtonNext.Content = "Next";
            if (TaskCompletionTime.Count > 0)
            {
                TaskCompletionTime.RemoveAt(TaskCompletionTime.Count - 1);
            }
            if (currentInstruction == 0)
            {
                ButtonBack.IsEnabled = false;
            }
            SetRichTextBox(instructions[currentInstruction], true);
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            ButtonBack.IsEnabled = true;
            currentInstruction++;
            TimeSpan ts = screenCapture.Statistics.Duration;
            TaskCompletionTime.Add(ts.Hours * 60 * 60 + ts.Minutes * 60 + ts.Seconds);
            if (currentInstruction == instructions.Count)
            {
                TaskCompletionTime.RemoveAt(TaskCompletionTime.Count - 1);
                currentInstruction--;
                screenCapture.Pause();
                ButtonStart.Content = "Start";
                currentState = RecordingState.Paused;
                MsgBox msgBox = new MsgBox("Capture Confirmation", "Are you finish with the tasks?", MsgBox.MsgBoxType.YesNo);
                msgBox.ShowDialog();
                if (msgBox.Result)
                {
                    timer.Stop();
                    screenCapture.Stop();
                    //mouseForm.Capture = false;
                    //mouseForm.Close();
                    this.Hide();
                }
            }
            if (currentInstruction == instructions.Count - 1)
            {
                ButtonNext.Content = "End";
                ButtonNext.Background = Brushes.LightGreen;
            }
            SetRichTextBox(instructions[currentInstruction], true);
        }

        private void Hide()
        {
            ((Storyboard)this.Resources["FadeOut"]).Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            MsgBox msgBox = new MsgBox("Review Video", "Please review your recording", MsgBox.MsgBoxType.Ok);
            msgBox.ShowDialog();
            ScreenCaptureDone();
        }

        private void ScreenCaptureDone()
        {
            if (OnScreenCaptureComplete == null) return;
            EventArgs args = new EventArgs();
            OnScreenCaptureComplete(this, args);
        }
    }
}