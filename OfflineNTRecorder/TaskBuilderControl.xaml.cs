﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Interaction logic for TaskBuilderControl.xaml
    /// </summary>
    public partial class TaskBuilderControl : UserControl
    {
        public delegate void TaskBuildCompletedHandler(object sender, JobArgs e);
        public event TaskBuildCompletedHandler OnTaskBuildComplete;

        JobDetails MainDetails;
        ErrorLogger errorLogger = new ErrorLogger("MainLog.log", true, "(Task Builder)");

        string tempFilename;
        List<TextBox> allTaskBox = new List<TextBox>();

        public TaskBuilderControl(JobDetails details)
        {
            InitializeComponent();

            MainDetails = details;
            allTaskBox.Add(TaskBox1);

            if (MainDetails.AddSurvey)
            {
                MainDetails.AddSurvey = true;
                SurveyCheckbox.IsChecked = true;
                ButtonProceed.Content = "Proceed to Survey";
            }
            else
            {
                MainDetails.AddSurvey = false;
                SurveyCheckbox.IsChecked = false;
                ButtonProceed.Content = "Save Profile";
            }         

            if (MainDetails.EditProfile)
            {
                ProfileNameBox.Text = Path.GetFileName(MainDetails.TestProfile).Replace(".txt","");

                if (MainDetails.Url != "<-Disable->")
                {
                    URLBox.Text = MainDetails.Url;
                }

                ScenarioBox.Text = MainDetails.Scenario;

                for (int i = 0; i < MainDetails.Tasks.Count; i++)
                {
                    MainDetails.Tasks[i] = (string)MainDetails.Tasks[i].Replace(@"\line ","\r\n");
                }
                TaskBox1.Text = MainDetails.Tasks[0];

                for (int i = 1; i < MainDetails.Tasks.Count; i++)
                {
                    AddTaskBox(MainDetails.Tasks[i]);
                }

                
            }
        }

        private void ButtonAddTask_Click(object sender, RoutedEventArgs e)
        {
            AddTaskBox("");
        }

        private void AddTaskBox(string text)
        {
            TextBlock taskLabel = new TextBlock()
            {
                Text = "Task " + (allTaskBox.Count + 1),
                Margin = new Thickness(10, 5, 10, 0)
            };

            TextBox taskBox = new TextBox()
            {
                Text = text,
                AcceptsReturn = true,
                VerticalScrollBarVisibility = ScrollBarVisibility.Visible,
                Height = 74,
                Margin = new Thickness(10, 5, 10, 0),
                TextWrapping = TextWrapping.Wrap
            };

            TaskStackPanel.Children.Add(taskLabel);
            TaskStackPanel.Children.Add(taskBox);

            allTaskBox.Add(taskBox);
        }

        private void ButtonProceed_Click(object sender, RoutedEventArgs e)
        {
            FullGrid.IsEnabled = false;

            string msg = CheckForError();
            string taskString = "";

            if (msg == null)
            {
                if (!MainDetails.AddSurvey)
                {
                    MsgBox confirmationBox = new MsgBox("Disable Survey Confirmation", "Are you sure you don't want to include a survey?", MsgBox.MsgBoxType.YesNo);
                    confirmationBox.ShowDialog();
                    if (!confirmationBox.Result)
                    {
                        FullGrid.IsEnabled = true;
                        return;
                    }
                }

                MainDetails.TestProfile = tempFilename;

                if (URLBox.Text == "")
                {
                    URLBox.Text = "<-Disable->";
                }             

                for (int i = 0; i < allTaskBox.Count; i++)
                {
                    if (allTaskBox[i].Text != "")
                    {
                        taskString += allTaskBox[i].Text.Replace("\r\n", @"\line ");
                        if (i != (allTaskBox.Count - 1))
                        {
                            taskString += "<-Next->";
                        }
                    }
                }

                try
                {
                    using (StreamWriter newProfile = new StreamWriter(MainDetails.TestProfile))
                    {
                        newProfile.WriteLine("url=" + URLBox.Text);
                        newProfile.WriteLine("scenario=" + ScenarioBox.Text);
                        newProfile.WriteLine("task=" + taskString);

                        if (!MainDetails.AddSurvey)
                        {
                            newProfile.Write("survey=<-Disable->");
                        }
                    }
                }
                catch (Exception exception)
                {
                    errorLogger.ExceptionLog(exception);
                    MainDetails.Error = true;
                }                            

                // Proceed to Survey here. Temporarily just go back to Main
                ((Storyboard)this.Resources["FadeOut"]).Begin();
                OnTaskBuildComplete?.Invoke(this, new JobArgs(MainDetails));
            }
            else
            {
                MsgBox msgBox = new MsgBox("Error", msg, MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();
                if (msgBox.Replay)
                {
                    FullGrid.IsEnabled = true;
                }
            }
        }

        private string CheckForError()
        {
            string message = null;
            tempFilename = MainDetails.MainFolder + ProfileNameBox.Text + ".txt";

            if (ProfileNameBox.Text == "")
            {
                message = "Please provide a profile name";
            }
            else if (InvalidFilename(ProfileNameBox.Text))
            {
                message = "Profile name can't contain any of the following:\r\n\\ / : * ? \" < > |";
            }
            else if (File.Exists(tempFilename) && !MainDetails.EditProfile)
            {
                message = "Profile name already in use";
            }
            else if (ScenarioBox.Text == "")
            {
                message = "Please provide a scenario";
            }
            else if (TaskBox1.Text == "")
            {
                message = "Please provide at least one task";
            }

            return message;
        }

        private bool InvalidFilename(string testName)
        {
            Regex containsInvalidCharacter = new Regex("["
                  + Regex.Escape(new string(Path.GetInvalidFileNameChars())) + "]");

            return containsInvalidCharacter.IsMatch(testName);
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            MsgBox msgBox = new MsgBox("Exit Confirmation", "Are you sure you want to go back?\r\nAll progress will be lost", MsgBox.MsgBoxType.YesNo);
            msgBox.ShowDialog();
            if (msgBox.Result)
            {
                MainDetails.AddSurvey = false;

                ((Storyboard)this.Resources["FadeOut"]).Begin();
                OnTaskBuildComplete?.Invoke(this, new JobArgs(MainDetails));
            }
        }

        private void TaskBuildMessages(string title, string msg, MsgBox.MsgBoxType boxType)
        {
            MsgBox msgBox = new MsgBox(title, msg, boxType);
            msgBox.ShowDialog();
        }

        private void TaskBuilder_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }

        private void SurveyCheckbox_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)SurveyCheckbox.IsChecked)
            {
                ButtonProceed.Content = "Proceed to Survey";
                MainDetails.AddSurvey = true;
            }
            else
            {
                ButtonProceed.Content = "Save Profile";
                MainDetails.AddSurvey = false;
            }
        }
    }
}
