﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Retrieves job details with user key in email
    /// </summary>
    public partial class MainControl : UserControl
    {
        public delegate void MainCompletedHandler(object sender, JobArgs e);
        public event MainCompletedHandler OnMainComplete;

        int selection;
        string selectedProfileName;

        JobDetails MainDetails;
        ErrorLogger errorLogger = new ErrorLogger("MainLog.log", true, "(Login)");

        public MainControl(JobDetails details)
        {
            InitializeComponent();

            MainDetails = details;
            MainDetails.AddProfile = false;
            MainDetails.EditProfile = false;
            MainDetails.AddSurvey = true;

            selectedProfileName = Path.GetFileName(MainDetails.TestProfile);
            FindProfiles();
        }

        private void FindProfiles()
        {
            string[] files = Directory.GetFiles(MainDetails.MainFolder, "*.txt");

            for (int i = 0; i < files.Length; i++)
            {
                files[i] = Path.GetFileName(files[i]);
                if (files[i] == selectedProfileName)
                {
                    selection = i;
                }
            }
            ComboBoxTestProfile.ItemsSource = files;
            ComboBoxTestProfile.SelectedIndex = selection;
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            TextBoxTesterName.Focus();
        }

        // Begin retrieving job
        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            StartJob();
        }

        // Close the whole application
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            MsgBox msgBox = new MsgBox("Exit Confirmation", "Are you sure you want to quit the application?", MsgBox.MsgBoxType.YesNo);
            msgBox.ShowDialog();
            if (msgBox.Result)
            {
                Window.GetWindow(this).Close();
            }
        }

        // Allow the "Enter" key to retrieve job
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                StartJob();
            }
        }

        // Retrieve job detail with the email provided
        private void StartJob()
        {
            if (TextBoxTesterName.Text != "")
            {
                ComboBoxTestProfile.IsEnabled = false;
                TextBoxTesterName.IsEnabled = false;
                ButtonStart.IsEnabled = false;
                ButtonClose.IsEnabled = false;

                try
                {
                    if (!Directory.Exists(MainDetails.TestFilesFolder + TextBoxTesterName.Text))
                    {
                        if(!File.Exists(MainDetails.MainFolder + selectedProfileName))
                        {
                            MsgBox msgBox = new MsgBox("Error", "Selected Profile is Missing. Please Select Another", MsgBox.MsgBoxType.Ok);
                            msgBox.ShowDialog();

                            FindProfiles();
                        }
                        else
                        {
                            Directory.CreateDirectory(MainDetails.TestFilesFolder + TextBoxTesterName.Text);

                            MainDetails.TestProfile = MainDetails.MainFolder + selectedProfileName;
                            MainDetails.TesterName = TextBoxTesterName.Text;
                            MainDetails.TesterFolder = MainDetails.TestFilesFolder + MainDetails.TesterName + "\\";
                            MainDetails.MobileTestFile = MainDetails.TesterFolder + MainDetails.TesterName + ".wmv";
                            MainDetails.SurveyAnswerFile = MainDetails.TesterFolder + MainDetails.TesterName + "-survey.txt";

                            ((Storyboard)this.Resources["FadeOut"]).Begin();
                            OnMainComplete?.Invoke(this, new JobArgs(MainDetails));
                        }
                    }
                    else
                    {
                        MsgBox msgBox = new MsgBox("Error", "Tester name already in use", MsgBox.MsgBoxType.Ok);
                        msgBox.ShowDialog();
                    }
                }
                catch (Exception exception)
                {
                    errorLogger.ExceptionLog(exception);
                    MsgBox msgBox = new MsgBox("Error", "Oops! We are sorry, something went wrong. Please contact support.", MsgBox.MsgBoxType.Ok);
                    msgBox.ShowDialog();
                    if (msgBox.Replay)
                    {
                        Window.GetWindow(this).Close();
                    }
                }
            }
            else
            {
                MsgBox msgBox = new MsgBox("Error", "Tester name cannot be empty", MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();
            }

            ComboBoxTestProfile.IsEnabled = true;
            TextBoxTesterName.IsEnabled = true;
            ButtonStart.IsEnabled = true;
            ButtonClose.IsEnabled = true;
        }

        private void ButtonAddProfile_Click(object sender, RoutedEventArgs e)
        {
            MainDetails.AddProfile = true;
            ((Storyboard)this.Resources["FadeOut"]).Begin();
            OnMainComplete?.Invoke(this, new JobArgs(MainDetails));
        }

        private void ComboBoxTestProfile_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedProfileName = (string)ComboBoxTestProfile.SelectedItem;
        }

        private void ButtonEditProfile_Click(object sender, RoutedEventArgs e)
        {
            MainDetails.AddProfile = true;
            MainDetails.EditProfile = true;

            ComboBoxTestProfile.IsEnabled = false;
            TextBoxTesterName.IsEnabled = false;
            ButtonStart.IsEnabled = false;
            ButtonClose.IsEnabled = false;

            try
            {
                if (!File.Exists(MainDetails.MainFolder + selectedProfileName))
                {
                    MsgBox msgBox = new MsgBox("Error", "Selected Profile is Missing. Please Select Another", MsgBox.MsgBoxType.Ok);
                    msgBox.ShowDialog();

                    FindProfiles();

                    ComboBoxTestProfile.IsEnabled = true;
                    TextBoxTesterName.IsEnabled = true;
                    ButtonStart.IsEnabled = true;
                    ButtonClose.IsEnabled = true;
                }
                else
                {
                    MainDetails.TestProfile = MainDetails.MainFolder + selectedProfileName;

                    ((Storyboard)this.Resources["FadeOut"]).Begin();
                    OnMainComplete?.Invoke(this, new JobArgs(MainDetails));
                }
            }
            catch (Exception exception)
            {
                errorLogger.ExceptionLog(exception);
                MsgBox msgBox = new MsgBox("Error", "Oops! We are sorry, something went wrong. Please contact support.", MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();
                if (msgBox.Replay)
                {
                    Window.GetWindow(this).Close();
                }
            }
        }

        // Allow users to change login control location by dragging
        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }  
    }
}
