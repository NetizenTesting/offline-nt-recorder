﻿using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.Live;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace OfflineNTRecorder
{
    public enum MobileTestState
    {
        Idle,
        Preview,
        TestVideoPrepare,
        TestVideoCountdown,
        RecorderTest,
        Replaying,
        Tip1,
        Tip1Half,
        Tip2,
        Tip3,
        VideoCountdown,
        Scenario,
        Recording,
        Paused,
        Completed
    }

    public partial class MobileRecorderForm : Form
    {
        Collection<EncoderDevice> audioSources;
        Collection<EncoderDevice> videoSources;
        EncoderDevice videoDevice;
        LiveJob job = null;
        LiveDeviceSource deviceSource;

        JobDetails MainDetails;

        private MobileTestState currentState = MobileTestState.Idle;
        int currentInstruction = 0;
        public bool testSuccess = false;
        
        double rate = 15;
        double time = 0;

        int CountDown = 5;

        bool backColor = true;
        bool recording = false;
        bool startRecording = false;

        public MobileRecorderForm(JobDetails details)
        {
            InitializeComponent();

            MainDetails = details;

            audioSources = EncoderDevices.FindDevices(EncoderDeviceType.Audio);
            foreach (EncoderDevice device in audioSources)
            {
                if (device.Category == EncoderDeviceCategory.Capture)
                {
                    AudioDeviceComboBox.Items.Add(device.Name);
                }
            }
            AudioDeviceComboBox.SelectedIndex = 0;

            videoSources = EncoderDevices.FindDevices(EncoderDeviceType.Video);
            foreach (EncoderDevice device in videoSources)
            {
                if (device.Category == EncoderDeviceCategory.Capture)
                {
                    VideoDeviceComboBox.Items.Add(device.Name);
                }
            }
            VideoDeviceComboBox.SelectedIndex = 0;

            VideoDeviceComboBox.SelectedIndexChanged += VideoDeviceComboBox_SelectedIndexChanged;
            AudioDeviceComboBox.SelectedIndexChanged += AudioDeviceComboBox_SelectedIndexChanged;
        }

        private void MobileRecorderForm_Load(object sender, EventArgs e)
        {
            labelMainButton.Hide();  
            picMainNextArrow.Hide();

            this.AutoScaleMode = AutoScaleMode.Dpi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(this.CurrentAutoScaleDimensions.Width, this.CurrentAutoScaleDimensions.Height);
            this.AutoSize = false;

            //int initialStyle = GetWindowLong(this.Handle, -20);
            //SetWindowLong(this.Handle, -20, initialStyle | 0x80000 | 0x20);
            RtfUpdate("Selecting Webcam and Microphone for recording:", "Make sure the screen on the left displays a video of your mobile device before clicking next.");
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (job != null)
            {
                if (job.IsCapturing)
                {
                    picLoadingAnimationGif.Hide();
                    //time += 50;
                    time = job.NumberOfEncodedSamples + job.NumberOfDroppedSamples;
                    //time = deviceSource.SourcePropertiesSnapshot().
                    int second = (int)Math.Round(time / rate);
                    if (recording)
                    {
                        if (!startRecording)
                        {
                            labelRecordingTimer.Text = "00:00";
                            
                            labelPrevious.Show();
                            labelNext.Show();
                            labelTip.Show();
                            labelRecordingTimer.Show();
                            picPreviousArrow.Show();
                            picNextArrow.Show();
                            InstructionUpdate(currentInstruction);

                            startRecording = true;
                        }
                        else
                        {
                            labelRecordingTimer.Text = (second / 60).ToString("D2") + ":" + (second % 60).ToString("D2"); 
                        }
                        return;
                    }
                    if (time == 0)
                    {
                        RtfUpdate("Recording Test:", "00:00");
                    }
                    else
                    {
                        RtfUpdate("Recording Test:", (second / 60).ToString("D2") + ":" + (second % 60).ToString("D2"));
                    }
                    if (currentState == MobileTestState.RecorderTest)
                    {
                        if (time > 5 * rate)
                        {
                            job.StopEncoding();
                            time = 0;
                            RtfUpdate("Preview your video:", "If you can see your recording and hear your voice, proceed to the next step");
                            labelMainButton.Show();
                            labelMainButton.Text = "Next";
                            labelMainButton.ForeColor = Color.FromArgb(39, 169, 225);
                            picMainNextArrow.Show();
                            picMainNextArrow.Image = new Bitmap(Properties.Resources.blue_arrow);
                            currentState = MobileTestState.Replaying;
                            axWindowsMediaPlayer1.Size = panel1.Size;
                            axWindowsMediaPlayer1.Visible = true;
                            axWindowsMediaPlayer1.settings.setMode("loop", false);
                            axWindowsMediaPlayer1.stretchToFit = false;
                            axWindowsMediaPlayer1.URL = MainDetails.MobileTestFile;
                            axWindowsMediaPlayer1.Ctlcontrols.play();
                            labelPrevious.Show();
                            picPreviousArrow.Show();
                        }
                    }
                }
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImportAttribute("user32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImportAttribute("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private void PictureMouseDrag_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void LabelClose_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to quit?", "Quit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult.Yes == result)
            {
                this.Close();
            }
        }

        private void VideoDeviceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangePreview();
        }

        private void AudioDeviceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangePreview();
        }

        private void ChangePreview()
        {
            currentState = MobileTestState.Preview;
            VideoDeviceComboBox.Enabled = false;
            AudioDeviceComboBox.Enabled = false;

            if (job != null)
            {
                job.RemoveDeviceSource(deviceSource);
            }

            job = new LiveJob();
            job.ApplyPreset(LivePresets.VC1HighSpeedBroadband4x3);

            videoDevice = videoSources[VideoDeviceComboBox.SelectedIndex];
            deviceSource = job.AddDeviceSource(videoDevice, audioSources[AudioDeviceComboBox.SelectedIndex]);
            deviceSource.PickBestVideoFormat(new Size(800, 600), 666);
            deviceSource.PreviewWindow = new PreviewWindow(new HandleRef(panel1, panel1.Handle));
            job.ActivateSource(deviceSource);
            VideoDeviceComboBox.Enabled = true;
            AudioDeviceComboBox.Enabled = true;
            picMainNextArrow.Show();
            labelMainButton.Show();
        }

        private void LabelMainButton_Click(object sender, EventArgs e)
        {
            ProceedCLick();
        }


        private void PicMainNexArrow_Click(object sender, EventArgs e)
        {
            ProceedCLick();
        }

        private void ProceedCLick()
        {
            switch (currentState)
            {
                case MobileTestState.Preview:
                    labelMainButton.Text = "Start";
                    labelMainButton.ForeColor = Color.FromArgb(56, 180, 73);
                    picMainNextArrow.Image = new Bitmap(Properties.Resources.green_arrow);
                    AudioDeviceComboBox.Hide();
                    VideoDeviceComboBox.Hide();
                    RtfUpdate("Test record your video:", "Press \"Start\" to do a 5 second test video");
                    //panel2.Height = 130;
                    currentState = MobileTestState.TestVideoPrepare;
                    break;
                case MobileTestState.TestVideoPrepare:
                    picMainNextArrow.Hide();
                    labelMainButton.Hide();
                    labelCountDown.Show();
                    CountDown = 5;
                    timer2.Enabled = true;
                    timer2.Start();
                    labelCountDown.Text = CountDown.ToString();
                    currentState = MobileTestState.TestVideoCountdown;
                    break;
                case MobileTestState.Replaying:
                    StopVideo();
                    picPreviousArrow.Hide();
                    labelPrevious.Hide();
                    //richTextBox2.Height = 90;
                    RtfUpdate("Mobile Testing Tips:", "- Put your device on a solid colored background.\\line - Check if camera is focused onto device.\\line - Adjust lighting and your device to reduce glare and reflections.");
                    currentState = MobileTestState.Tip1;
                    break;
                case MobileTestState.Tip1:
                    RtfUpdate("Mobile Testing Tips:", "- You will not be able to pause the recording during the test. Please ensure that you switch your device to flight mode to ensure there is no interruption during the test \\line - Ensure the timer starts before begining your test");
                    if (MainDetails.Url != "<-Disable->")
                    {
                        currentState = MobileTestState.Tip1Half;
                    }
                    else
                    {
                        currentState = MobileTestState.Tip2;
                    }
                    break;
                case MobileTestState.Tip1Half:
                    RtfUpdate("App name and installation instruction:", @"Download From:\line " + MainDetails.Url);
                    currentState = MobileTestState.Tip2;
                    break;
                case MobileTestState.Tip2:
                    RtfUpdate("Test Scenario:", MainDetails.Scenario);
                    currentState = MobileTestState.Tip3;
                    labelMainButton.Text = "Start";
                    labelMainButton.ForeColor = Color.FromArgb(56, 180, 73);
                    picMainNextArrow.Image = new Bitmap(Properties.Resources.green_arrow);
                    break;
                case MobileTestState.Tip3:
                    currentState = MobileTestState.VideoCountdown;
                    //richTextBox2.Height = 75;
                    labelMainButton.Hide();
                    picMainNextArrow.Hide();
                    recording = true;
                    timer2.Enabled = true;
                    timer2.Start();
                    timer3.Enabled = true;
                    timer3.Start();

                    CountDown = 5;
                    labelCountDown.Show();
                    labelCountDown.Text = CountDown.ToString();
                    break;
                default:
                    break;
            }
        }

        void InstructionUpdate(int Number)
        {
            if (Number == 0)
            {
                labelPrevious.Text = "";
                labelPrevious.Enabled = false;
                picPreviousArrow.Hide();
                if (MainDetails.Tasks.Count == 1)
                {
                    picNextArrow.Hide();
                    labelNext.Text = "End";
                    labelNext.BackColor = Color.FromArgb(56, 180, 73);
                    labelNext.ForeColor = Color.White;
                }
            }
            else
            {
                labelPrevious.Text = "Previous";
                labelPrevious.Enabled = true;
                picPreviousArrow.Show();
            }

            labelMainHeader.Text = "Task " + (Number + 1).ToString() + " of " + MainDetails.Tasks.Count.ToString();
            RtfUpdate(Number + 1, MainDetails.Tasks[Number]);
        }

        void RtfUpdate(string title, string content)
        {
            richTextBox2.Rtf = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Calibri;}}{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang9\b\f0\fs30 " + title + @"\b0\line " + content + "}";
        }
        void RtfUpdate(int taskNumber, string content)
        {
            richTextBox2.Rtf = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Calibri;}}{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang9\f0\fs28 Task " + taskNumber.ToString() + @" : " + content + "}";
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            CountDown--;
            labelCountDown.Text = CountDown.ToString();
            if (CountDown == 0)
            {
                timer2.Stop();
                timer2.Enabled = false;
                labelCountDown.Text = "";
                labelCountDown.Hide();
                if (currentState == MobileTestState.TestVideoCountdown)
                {
                    RecorderTestRun();
                }
                else
                {
                    RecorderRun();
                }
            }
        }

        private void RecorderRun()
        {
            picLoadingAnimationGif.Show();
            job.StartEncoding();
            time = 0;
        }

        void RecorderTestRun()
        {
            currentState = MobileTestState.RecorderTest;
            rate = deviceSource.SourcePropertiesSnapshot().FrameRate;
            job.OutputFormat.VideoProfile.Size = deviceSource.SourcePropertiesSnapshot().Size;
            FileArchivePublishFormat fileOut = new FileArchivePublishFormat()
            {
                OutputFileName = MainDetails.MobileTestFile
            };
            job.PublishFormats.Add(fileOut);
            picLoadingAnimationGif.Show();
            job.StartEncoding();
            time = 0;
        }

        private void PicPreviousArrow_Click(object sender, EventArgs e)
        {
            BackPressed();
        }

        private void LabelPrevious_Click(object sender, EventArgs e)
        {
            BackPressed();
        }

        private void BackPressed()
        {
            if (currentState == MobileTestState.Completed)
            {
                StopVideo();
                labelPrevious.Text = "Previous";
                labelNext.Text = "Next";
                currentInstruction = 0;
                labelNext.BackColor = Color.FromArgb(93, 93, 93);
                labelNext.ForeColor = Color.FromArgb(145, 215, 238);
                labelMainHeader.Text = "Test Setup";
                labelRecordingTimer.Text = "--:--";
                RtfUpdate("Test Scenario:", MainDetails.Scenario);
                currentState = MobileTestState.Tip3;
                labelMainButton.Text = "Start";
                labelMainButton.ForeColor = Color.FromArgb(56, 180, 73);
                picMainNextArrow.Image = new Bitmap(Properties.Resources.green_arrow);
                labelMainButton.Show();
                picMainNextArrow.Show();
                labelPrevious.Hide();
                labelNext.Hide();
                labelTip.Hide();
                labelRecordingTimer.Hide();
                picPreviousArrow.Hide();
                picNextArrow.Hide();
                pictureBox6.Hide();
                return;
            }
            if (currentState == MobileTestState.Replaying)
            {
                StopVideo();
                job.RemoveDeviceSource(deviceSource);
                //panel2.Height = 184;
                picPreviousArrow.Hide();
                labelPrevious.Hide();
                AudioDeviceComboBox.Show();
                VideoDeviceComboBox.Show();
                currentState = MobileTestState.Idle;
                picMainNextArrow.Hide();
                labelMainButton.Hide();
                RtfUpdate("Selecting Webcam and Microphone for recording:", "Make sure the screen on the left displays a video of your mobile device before clicking next.");
            }
            else
            {
                currentInstruction--;
                picNextArrow.Show();
                labelNext.Text = "Next";
                labelNext.BackColor = Color.FromArgb(93, 93, 93);
                labelNext.ForeColor = Color.FromArgb(145, 215, 238);
                if (currentInstruction < 0)
                {
                    currentInstruction = 0;
                }
                InstructionUpdate(currentInstruction);
            }
        }

        void StopVideo()
        {
            axWindowsMediaPlayer1.Ctlcontrols.stop();
            axWindowsMediaPlayer1.close();
            axWindowsMediaPlayer1.Visible = false;
        }

        private void Timer3_Tick(object sender, EventArgs e)
        {
            if (backColor)
            {
                backColor = false;
                labelTip.BackColor = Color.FromArgb(238, 58, 64);
                labelTip.ForeColor = Color.White;
            }
            else
            {
                backColor = true;
                labelTip.BackColor = Color.FromArgb(205, 204, 203);
                labelTip.ForeColor = Color.Black;
            }
        }

        private void PicNextArrow_Click(object sender, EventArgs e)
        {
            NextPress();
        }

        private void LabelNext_Click(object sender, EventArgs e)
        {
            NextPress();
        }

        private void NextPress()
        {
            if (currentState == MobileTestState.Completed)
            {
                StopVideo();
                testSuccess = true;
                this.Close();
                return;
            }
            currentInstruction++;
            if (currentInstruction == MainDetails.Tasks.Count)
            {
                currentInstruction--;
                DialogResult result = MessageBox.Show("Are you sure you are done? (Video Recording is still on)", "Info", MessageBoxButtons.YesNo);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    job.StopEncoding();
                    time = 0;
                    currentState = MobileTestState.Replaying;
                    axWindowsMediaPlayer1.Visible = true;
                    axWindowsMediaPlayer1.settings.setMode("loop", false);
                    axWindowsMediaPlayer1.stretchToFit = false;
                    axWindowsMediaPlayer1.URL = MainDetails.MobileTestFile;
                    axWindowsMediaPlayer1.Ctlcontrols.play();
                    RtfUpdate("Please review your recording and click Proceed", "");
                    picPreviousArrow.Hide();
                    labelPrevious.Enabled = false;
                    labelPrevious.Text = "";
                    labelNext.Text = "Proceed";
                    currentState = MobileTestState.Completed;
                    return;
                }
            }
            else if (currentInstruction == MainDetails.Tasks.Count - 1)
            {
                picNextArrow.Hide();
                labelNext.Text = "End";
                labelNext.BackColor = Color.FromArgb(56, 180, 73);
                labelNext.ForeColor = Color.White;
            }
            InstructionUpdate(currentInstruction);
        }

        private void PictureBox6_Click(object sender, EventArgs e)
        {
        }

        private void Label12_Click(object sender, EventArgs e)
        {

        }

        private void MobileRecorderForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (recording)
            {
                job.StopEncoding();
            }
        }

        private void Panel3_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void PictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Label4_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
