﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Interaction logic for VideoEncoderControl.xaml
    /// </summary>
    /// 

    public class CompleteEventArgs : EventArgs
    {
        public bool Success { get; private set; }
        public string Error { get; private set; }

        public CompleteEventArgs(bool Success)
        {
            this.Success = Success;
            this.Error = "";
        }

        public CompleteEventArgs(bool Success, string Error)
        {
            this.Success = Success;
            this.Error = Error;
        }
    }

    public partial class VideoEncoderControl : UserControl
    {
        string inputVideoFile;
        string outputVideoFile;
        //Job encodingJob;
        BackgroundWorker worker = new BackgroundWorker();
        //Thread encodingThread;
        //private string outputDirectory;
        delegate void UpdateUIWithEvent(DataReceivedEventArgs Events);
        delegate void UpdateUIwithString(string info);
        delegate void UpdateUI();
        public delegate void EncoderCompletedHandler(object sender, CompleteEventArgs e);
        public event EncoderCompletedHandler OnEncoderComplete;
        ErrorLogger errorLogger = new ErrorLogger("EncoderLog.log");

        public VideoEncoderControl(string InputVideoFile, string OutputVideoFile)
        {
            this.InitializeComponent();
            this.inputVideoFile = InputVideoFile;
            this.outputVideoFile = OutputVideoFile;
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUI(encodingCompleted), null);
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Process exeProcess = new Process();
                exeProcess.StartInfo.CreateNoWindow = true;
                exeProcess.StartInfo.UseShellExecute = false;
                exeProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                exeProcess.StartInfo.FileName = "ffmpeg.exe";
                exeProcess.StartInfo.Arguments = "-y -i \"" + inputVideoFile + "\" -pix_fmt yuv420p -c:v libx264 -preset:v slow -profile:v baseline -x264opts level=3.0:ref=1 -b:v 700k -r:v 25/1 -force_fps -movflags +faststart -pass 1 \"" + outputVideoFile + "\"";
                exeProcess.StartInfo.RedirectStandardError = true;
                exeProcess.ErrorDataReceived += exeProcess_ErrorDataReceived;
                exeProcess.Start();
                exeProcess.BeginErrorReadLine();
                exeProcess.WaitForExit();
            }
            catch (Exception err)
            {
                this.Dispatcher.BeginInvoke(new UpdateUIwithString(Errordetected), err.Message);
            }
            //try
            //{
            //    using (Job encodingJob = new Job())
            //    {
            //        MediaItem encodingItem = new MediaItem(inputVideoFile);
            //        //vProfile.Bitrate = new VariableConstrainedBitrate();
            //        encodingItem.ApplyPreset(Presets.VC1HD1080pVBR);
            //        encodingItem.OutputFormat.VideoProfile.AspectRatio = null;
            //        encodingItem.OutputFormat.VideoProfile.AutoFit = false;
            //        encodingItem.OutputFormat.VideoProfile.Bitrate = new VariableConstrainedBitrate(2048, 2048 * 2);
            //        encodingItem.OutputFormat.VideoProfile.NumberOfEncoderThreads = 0;
            //        encodingItem.OutputFormat.VideoProfile.Size = encodingItem.OriginalVideoSize;
            //        encodingItem.OutputFormat.VideoProfile.SmoothStreaming = false;
            //        encodingItem.OutputFormat.AudioProfile.Bitrate = new VariableConstrainedBitrate(48, 96);
            //        encodingItem.OutputFileName = outputVideoFile;
            //        //encodingItem.OutputFormat.VideoProfile = vProfile;
            //        //encodingItem.OutputFormat.AudioProfile = aProfile;

            //        encodingJob.MediaItems.Add(encodingItem);
            //        encodingJob.OutputDirectory = outputDirectory;
            //        encodingJob.CreateSubfolder = false;
            //        encodingJob.EncodeProgress += encodingJob_EncodeProgress;
            //        encodingJob.Encode();
            //        encodingJob.MediaItems.Clear();
            //    }
            //    this.Dispatcher.BeginInvoke(new UpdateUI(encodingCompleted), null);
            //}
            //catch (EncodeErrorException err)
            //{
            //    this.Dispatcher.BeginInvoke(new UpdateUIwithString(Errordetected), err.Message);
            //}
        }

        private void exeProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUIWithEvent(updateProgress), e);
        }

        public void encodingCompleted()
        {
            ProgressBlock.Text = "Encoding Completed";
            encoderDone();
        }

        private void encoderDone()
        {
            errorLogger.AppendLog("Encoding Completed:" + percent.ToString());
            if (OnEncoderComplete == null) return;
            bool success = true;
            CompleteEventArgs eventArgs = new CompleteEventArgs(success);
            if (percent < 90)
            {
                eventArgs = new CompleteEventArgs(false, logInfo);
            }
            OnEncoderComplete(this, eventArgs);
        }

        //void encodingJob_EncodeProgress(object sender, EncodeProgressEventArgs e)
        //{
        //    //this.Dispatcher.BeginInvoke(new UpdateUIWithEvent(updateProgress), e);
        //}


        double totalTime = 1;
        double percent = 0;
        string logInfo = "";
        string encodingStr = "Encoding";

        void updateProgress(DataReceivedEventArgs e)
        {
            // This is invariant
            NumberFormatInfo format = new NumberFormatInfo();
            // Set the 'splitter' for thousands
            format.NumberGroupSeparator = ",";
            // Set the decimal seperator
            format.NumberDecimalSeparator = ".";

            if (e.Data != null)
            {
                errorLogger.AppendLog(e.Data);
                logInfo += e.Data + Environment.NewLine;
                try
                {
                    if (e.Data.StartsWith("  Duration:"))
                    {
                        string[] infos = e.Data.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        string[] times = infos[1].Split(new string[] { ":", "," }, StringSplitOptions.RemoveEmptyEntries);
                        totalTime = double.Parse(times[2], format);
                        totalTime += double.Parse(times[1], format) * 60;
                        totalTime += double.Parse(times[0], format) * 60 * 60;
                    }
                    if (e.Data.StartsWith("frame="))
                    {
                        string[] infos = e.Data.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string info in infos)
                        {
                            if (info.StartsWith("time="))
                            {
                                //Console.Write(info.Remove(0, 5) + " ");
                                string[] times = info.Remove(0, 5).Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                                double time = double.Parse(times[2], format);
                                time += double.Parse(times[1], format) * 60;
                                time += double.Parse(times[0], format) * 60 * 60;
                                //Console.WriteLine("{0} {1} {2}%", time.ToString("0000000.00"), totalTime.ToString("0000000.00"), (time / totalTime * 100).ToString("0000000.00"));
                                ProgressBlock.Text = (time / totalTime).ToString("P2");
                                RecordingProgressBar.Value = (time / totalTime * 100);
                                percent = time / totalTime * 100;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    ProgressBlock.Text = encodingStr + ".";
                    if (encodingStr.Length == 12)
                    {
                        encodingStr = "Encoding";
                    }
                }
            }
            //ProgressBlock.Text = "Encoding pass " + e.CurrentPass.ToString() + "/" + e.TotalPasses + " - " + e.CurrentPosition.Minutes.ToString("d2") + ":" + e.CurrentPosition.Seconds.ToString("d2") + " encoded";
            //RecordingProgressBar.Value = e.Progress;
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            //jobSetup.RunWorkerAsync();
            //encodingThread = new Thread(new ThreadStart(EncodeThread));
            //encodingThread.Start();
            worker.RunWorkerAsync();
        }

        private void EncodeThread()
        {

        }

        private void Errordetected(string ErrorMessage)
        {
            MsgBox msgBox = new MsgBox("Encoding Error", ErrorMessage + ". Press Ok Exit.", MsgBox.MsgBoxType.Ok);
            msgBox.ShowDialog();
            Window.GetWindow(this).Close();
            //worker.RunWorkerAsync();
        }

        private void EncoderControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }
    }
}