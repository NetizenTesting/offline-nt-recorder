﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Check if NT Recorder is already running.
    /// Close the app if yes.
    /// </summary>
    public partial class App : Application
    {
        // Overrides OnStartup in Application class
        protected override void OnStartup(StartupEventArgs e)
        {
            IsAppAlreadyRunning();
        }

        // Check if Netizen Testing Recorder is already running
        private static void IsAppAlreadyRunning()
        {
            Process currentProcess = Process.GetCurrentProcess();

            if (Process.GetProcessesByName(currentProcess.ProcessName).Any(p => p.Id != currentProcess.Id))
            {
                MessageBox.Show("Netizen Testing Recorder is already running.", "Application already running",
                MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Current.Shutdown(0);
                return;
            }
        }
    }
}
