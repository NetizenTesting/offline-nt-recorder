﻿using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.Live;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetizenTestingDesktop
{
    public enum MobileTestState
    {
        Preview,
        RecorderTest,
        Replaying,
        Scenario,
        Recording,
        Paused,
        Completed
    }
    public partial class MobileRecorderForm : Form
    {

        Collection<EncoderDevice> audioSources;
        Collection<EncoderDevice> videoSources;
        EncoderDevice videoDevice;
        LiveJob job = null;
        LiveDeviceSource deviceSource;
        private string scenario = "Test Scene", fileLocation;
        private List<string> instructions = new List<string>() { "T1", "T2", "T3"};
        private MobileTestState currentState = MobileTestState.Preview;
        int currentInstruction = 0;
        public bool testSuccess = false;

        public MobileRecorderForm(string Filename, string Scenario, List<string> Instructions)
        {
            this.scenario = Scenario;
            this.instructions = Instructions;
            this.fileLocation = Filename;
            InitializeComponent();
            audioSources = EncoderDevices.FindDevices(EncoderDeviceType.Audio);
            foreach (EncoderDevice device in audioSources)
            {
                if (device.Category == EncoderDeviceCategory.Capture)
                {
                    AudioDeviceComboBox.Items.Add(device.Name);
                }
            }
            AudioDeviceComboBox.SelectedIndex = 0;
            videoSources = EncoderDevices.FindDevices(EncoderDeviceType.Video);
            foreach (EncoderDevice device in videoSources)
            {
                if (device.Category == EncoderDeviceCategory.Capture)
                {
                    VideoDeviceComboBox.Items.Add(device.Name);
                }
            }
            VideoDeviceComboBox.SelectedIndex = 0;
        }

        private void MobileRecorderForm_Load(object sender, EventArgs e)
        {

        }

        double rate = 30;

        private void button2_Click(object sender, EventArgs e)
        {
            switch (currentState)
            {
                case MobileTestState.Preview:
                    button4.Enabled = true;
                    if (job != null)
                    {
                        job.RemoveDeviceSource(deviceSource);
                    }
                    job = new LiveJob();
                    job.ApplyPreset(LivePresets.VC1HighSpeedBroadband4x3);
                    videoDevice = videoSources[VideoDeviceComboBox.SelectedIndex];
                    deviceSource = job.AddDeviceSource(videoDevice, audioSources[AudioDeviceComboBox.SelectedIndex]);
                    deviceSource.PreviewWindow = new PreviewWindow(new HandleRef(panel1, panel1.Handle));
                    job.ActivateSource(deviceSource);
                    updateText("To reselect device, use the combo box at the top and click on preview.\nClick Next to proceed with the test.");
                    button3.Enabled = true;
                    break;
                case MobileTestState.RecorderTest:
                    label3.Text = "Preparing Capture";
                    rate = deviceSource.SourcePropertiesSnapshot().FrameRate;
                    job.OutputFormat.VideoProfile.Size = deviceSource.SourcePropertiesSnapshot().Size;
                    FileArchivePublishFormat fileOut = new FileArchivePublishFormat();
                    fileOut.OutputFileName = fileLocation;
                    job.PublishFormats.Add(fileOut);
                    job.StartEncoding();
                    time = 0;
                    button2.Enabled = false;
                    button3.Enabled = false;
                    break;
                case MobileTestState.Scenario:
                    label3.Text = "Preparing Capture";
                    job.StartEncoding();
                    time = 0;
                    button2.Enabled = false;
                    button1.Enabled = false;
                    button3.Enabled = true;
                    currentState = MobileTestState.Recording;
                    updateText("Task " + (currentInstruction + 1).ToString() + ": " + instructions[currentInstruction]);
                    break;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            switch (currentState)
            {
                case MobileTestState.Preview:
                    button4.Enabled = false;
                    AudioDeviceComboBox.Enabled = false;
                    VideoDeviceComboBox.Enabled = false;
                    currentState = MobileTestState.RecorderTest;
                    updateText("Click Start to do a 5 second test Video.");
                    button2.Text = "Start";
                    button3.Enabled = false;
                    break;
                case MobileTestState.Replaying:
                    currentState = MobileTestState.Scenario;
                    axWindowsMediaPlayer1.Ctlcontrols.stop();
                    axWindowsMediaPlayer1.close();
                    axWindowsMediaPlayer1.Visible = false;
                    updateText(scenario);
                    MessageBox.Show("You will not be able to pause the recording during the test. Please ensure that you switch your device to flight mode to ensure there is no interruption during the test", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    button2.Enabled = true;
                    button1.Enabled = false;
                    button3.Enabled = false;
                    break;
                case MobileTestState.Scenario:
                    break;
                case MobileTestState.Completed:
                    testSuccess = true;
                    axWindowsMediaPlayer1.Ctlcontrols.stop();
                    axWindowsMediaPlayer1.close();
                    axWindowsMediaPlayer1.Visible = false;
                    this.Close();
                    break;
                case MobileTestState.Recording:
                    button1.Enabled = true;
                    currentInstruction++;
                    if (currentInstruction == instructions.Count)
                    {
                        currentInstruction--;
                        DialogResult result = MessageBox.Show("Are you sure you are done? (Video Recording is still on)", "Info", MessageBoxButtons.YesNo);
                        if (result == System.Windows.Forms.DialogResult.Yes)
                        {
                            button3.Text = "Next";
                            job.StopEncoding();
                            updateText("Click Next to proceed.\nClick Back to redo.");
                            time = 0;
                            currentState = MobileTestState.Replaying;
                            axWindowsMediaPlayer1.Visible = true;
                            //axWindowsMediaPlayer1.settings.setMode("loop", true);
                            axWindowsMediaPlayer1.stretchToFit = false;
                            axWindowsMediaPlayer1.URL = fileLocation;
                            axWindowsMediaPlayer1.Ctlcontrols.play();
                            button1.Enabled = true;
                            button3.Enabled = true;
                            currentState = MobileTestState.Completed;
                            return;
                        }
                    }
                    else if (currentInstruction == instructions.Count-1)
                    {
                        button3.Text = "End";
                    }
                    updateText("Task " + (currentInstruction + 1).ToString() + ": " + instructions[currentInstruction]);
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (currentState)
            {
                case MobileTestState.Replaying:
                    currentState = MobileTestState.RecorderTest;
                    updateText("Click Start to do a 5 second test Video.");
                    button2.Text = "Start";
                    axWindowsMediaPlayer1.Ctlcontrols.stop();
                    axWindowsMediaPlayer1.close();
                    axWindowsMediaPlayer1.Visible = false;
                    button1.Enabled = false;
                    button2.Enabled = true;
                    button3.Enabled = false;
                    break;
                case MobileTestState.Recording:
                    currentInstruction--;
                    button3.Text = "Next";
                    if (currentInstruction == 0)
                    {
                        button1.Enabled = false;
                    }
                    updateText("Task " + (currentInstruction + 1).ToString() + ": " + instructions[currentInstruction]);
                    break;
                case MobileTestState.Completed:
                    currentState = MobileTestState.Scenario;
                    currentInstruction = 0;
                    axWindowsMediaPlayer1.Ctlcontrols.stop();
                    axWindowsMediaPlayer1.close();
                    axWindowsMediaPlayer1.Visible = false;
                    updateText(scenario);
                    button2.Enabled = true;
                    button1.Enabled = false;
                    button3.Enabled = false;
                    break;
            }
        }
        double time = 0;

        void updateText(string text)
        {
            richTextBox1.Text = text;
            richTextBox1.SelectionStart = 0;
            richTextBox1.ScrollToCaret();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (job != null)
            {
                if (job.IsCapturing)
                {
                    //time += 50;
                    time = job.NumberOfEncodedSamples + job.NumberOfDroppedSamples;
                    //time = deviceSource.SourcePropertiesSnapshot().
                    int second = (int) Math.Round(time/rate);
                    if (time == 0)
                    {
                        label3.Text = "Preparing Capture";
                    }
                    else
                    {
                        label3.Text = (second / 60).ToString("D2") + ":" + (second % 60).ToString("D2");
                    }
                    if (currentState == MobileTestState.RecorderTest)
                    {
                        if (time > 5 * rate)
                        {
                            job.StopEncoding();
                            time = 0;
                            MessageBox.Show("Please review your video.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            currentState = MobileTestState.Replaying;
                            axWindowsMediaPlayer1.Visible = true;
                            //axWindowsMediaPlayer1.settings.setMode("loop", true);
                            axWindowsMediaPlayer1.stretchToFit = false;
                            axWindowsMediaPlayer1.URL = fileLocation;
                            axWindowsMediaPlayer1.Ctlcontrols.play(); 
                            updateText("Click Next to proceed.\nClick Back to redo 5 second test video.");
                            button1.Enabled = true;
                            button3.Enabled = true;
                        }
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (deviceSource != null)
            {
                if (deviceSource.IsDialogSupported(ConfigurationDialog.VideoCapturePinDialog))
                {
                    deviceSource.ShowConfigurationDialog(ConfigurationDialog.VideoCapturePinDialog, (new HandleRef(panel1, panel1.Handle)));
                }
            }
        }

    }
}
