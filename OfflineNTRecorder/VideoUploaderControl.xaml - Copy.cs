﻿using S3Uploader;
//using Amazon.Runtime;
//using Amazon.S3;
//using Amazon.S3.Model;
//using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;


namespace NetizenTestingDesktop
{
    /// <summary>
    /// Interaction logic for VideoEncoderControl.xaml
    /// </summary>
    public partial class VideoUploaderControl : UserControl
    {
        string uploadVideoFile;
        string uploadFileName;
        BackgroundWorker uploadWorker = new BackgroundWorker();
        public delegate void UploadCompletedHandler(object sender, CompleteEventArgs e);
        public event UploadCompletedHandler OnUploadComplete;
        delegate void UpdateUIDouble(double percent);
        delegate void UpdateUI();
        Uploader uploader;

        public VideoUploaderControl(string InputVideoFile, string UploadFileName)
        {
            this.InitializeComponent();
            this.uploadVideoFile = InputVideoFile;
            this.uploadFileName = UploadFileName;
        }

        int Retry = 0;

        void uploader_OnUpdateStatus(object sender, Uploader.UploaderEventArgs e)
        {
            switch (e.Status)
            {
                case Uploader.UploaderStatus.SetupFailed:
                    this.Dispatcher.BeginInvoke(new UpdateUI(RetryUpload));
                    break;
                case Uploader.UploaderStatus.UploadComplete:
                    this.Dispatcher.BeginInvoke(new UpdateUI(UploadComplete));
                    break;
                case Uploader.UploaderStatus.UploadFailed:
                    this.Dispatcher.BeginInvoke(new UpdateUI(RetryUpload));
                    break;
                case Uploader.UploaderStatus.Uploading:
                    this.Dispatcher.BeginInvoke(new UpdateUIDouble(UpdatePercent), e.Percent);
                    break;
            }
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            uploader = new Uploader(uploadVideoFile, uploadFileName);
            uploader.OnUpdateStatus += uploader_OnUpdateStatus;
            uploader.Upload();
        }

        void UpdatePercent(double percent)
        {
            ProgressBlock.Text = percent.ToString("p2");
            ProgressBar.Value = percent * 100;
        }

        void UploadComplete()
        {
            ProgressBlock.Text = "Finalizing Upload";
            if (OnUploadComplete == null) return;
           bool success = true;
            CompleteEventArgs eventArgs = new CompleteEventArgs(success);
            OnUploadComplete(this, eventArgs);
        }

        void RetryUpload()
        {
            Retry++;
            if (Retry == 6)
            {
                MessageBox.Show("Connection time out. Launch the application again when having better connection.", "Upload failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Window.GetWindow(this).Close();
            }
            uploader = new Uploader(uploadVideoFile, uploadFileName);
            uploader.OnUpdateStatus += uploader_OnUpdateStatus;
            uploader.Upload();
            MainTextBlock.Text = "Retrying upload " + Retry.ToString() + " of 5";
        }

        private void EncoderControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }
    }
}