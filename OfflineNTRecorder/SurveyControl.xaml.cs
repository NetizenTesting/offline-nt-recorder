﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OfflineNTRecorder
{
	/// <summary>
	/// Interaction logic for SurveyControl.xaml
	/// </summary>
	public partial class SurveyControl : UserControl
	{
        public delegate void SurveyCompletedHandler(object sender, JobArgs e);
        public event SurveyCompletedHandler OnSurveyComplete;

        JobDetails MainDetails;
        List<string> surveyAnswers = new List<string>() { "A1", "A2", "A3", "A4" };

		public SurveyControl(JobDetails details)
		{
			InitializeComponent();

            MainDetails = details;

            Question1.Text = MainDetails.SurveyQuestions[0];
            Question2.Text = MainDetails.SurveyQuestions[1];
            Question3.Text = MainDetails.SurveyQuestions[2];
            Question4.Text = MainDetails.SurveyQuestions[3];
		}

        void UpdateStatus()
        {
            if (OnSurveyComplete == null) return;

            surveyAnswers[0] = Answer1.Text;
            surveyAnswers[1] = Answer2.Text;
            surveyAnswers[2] = Answer3.Text;
            surveyAnswers[3] = Answer4.Text;

            MainDetails.SurveyAnswers = surveyAnswers;

            OnSurveyComplete(this, new JobArgs(MainDetails));
        }

        private void ButtonSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (Answer1.Text.Length == 0 || Answer2.Text.Length == 0 || Answer3.Text.Length == 0 || Answer4.Text.Length == 0)
            {
                MsgBox msgBox = new MsgBox("Submit Error", "All question must be answered.", MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();
            }
            else
            {
                MsgBox msgBox = new MsgBox("Proceed for submission?", "Your answer will be submitted as is, so please ensure that they are accurate.", MsgBox.MsgBoxType.YesNo);
                msgBox.ShowDialog();
                if (msgBox.Result)
                {
                    UpdateStatus();
                }
            }
        }

        private void SurveyUesrControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            double height = MyStackPanel.ActualHeight;
            MyScrollViewer.UpdateLayout();
        }
	}
}