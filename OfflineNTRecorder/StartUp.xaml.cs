﻿using System;
using System.ComponentModel;
using System.IO;
using System.Timers;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Retrieves Default profile from Settings and create if not available
    /// </summary>

    // A Custom Event Arguments for Job Details
    public class JobArgs : EventArgs
    {
        public JobDetails Details { get; private set; }

        public JobArgs(JobDetails details)
        {
            Details = details;
        }
    }

    public partial class StartUp : UserControl
    {
        Timer loadingTimer = new Timer(200);
        delegate void UpdateUI();   

        public delegate void StartUpCompletedHandler(object sender, JobArgs e);
        public event StartUpCompletedHandler OnStartUpComplete;

        JobDetails MainDetails;
        BackgroundWorker worker = new BackgroundWorker();
        ErrorLogger errorLogger = new ErrorLogger("MainLog.log", true, "(Startup)");

        public StartUp(JobDetails details)
        {
            this.InitializeComponent();
            loadingTimer.Elapsed += LoadingTimer_Elapsed;
            loadingTimer.Start();

            MainDetails = details;

            if (!File.Exists(MainDetails.TestProfile))
            {
                using (StreamWriter file = new StreamWriter(MainDetails.TestProfile))
                {
                    file.WriteLine("url=<-Disable->");
                    file.WriteLine("scenario=This is a test for the Mobile recorder.");
                    file.WriteLine(@"task=First, go to your phone's browser and visit our website at: \line \line http://www.netizentesting.com<-Next->" +
                        "While browsing through your phone, announce what you tap and which direction you swipe for about 30 seconds. (This is to check the audio and video sync)<-Next->" +
                        "Play around the new recorder interface. Please note anything that confuse you or not working properly or not working as you expected it to do.<-Next->" +
                        "You can end this test now. Thank You for your time.");
                    file.WriteLine("survey=During recording, is there any lag on the live preview video? if yes, how does it affect you doing the test?<-Next->" +
                        "Did the video play properly? Can you hear yourself?<-Next->" +
                        "Did you notice any problems with this interface?<-Next->" +
                        "Did you notice a red bar appearing? is it helpful to you?");
                }
            }

            if (!File.Exists(MainDetails.SettingsFile))
            {
                using (StreamWriter settings = new StreamWriter(MainDetails.SettingsFile))
                {
                    settings.Write("profile=Default Profile.txt");
                }
            }
        }

        // Loading text animation (async)
        private void LoadingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUI(UpdateTextBlock), null);
        }
  
        private void UpdateTextBlock()
        {
            if (LoadingText.Text.Length == 10)
            {
                LoadingText.Text = "Loading";
            }
            else
            {
                LoadingText.Text += ".";
            }
        }

        // Begin running background worker once startup control has faded in
        private void FadeInAnimation_Completed(object sender, EventArgs e)
        {
            worker.DoWork += Worker_DoWork;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        // Retrieve Profile Settings
        void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string settings = File.ReadAllText(MainDetails.SettingsFile);

            string[] settingDetails = settings.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

            if (settingDetails[0] == "profile")
            {
                MainDetails.TestProfile = settingDetails[1];
            } 
        }

        // Hide startup control when background worker completes
        void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loadingTimer.Stop();
            LoadingText.Text = "Completed";

            this.Hide();
        }

        // Induce fade out
        private void Hide()
        {
            ((Storyboard)this.Resources["FadeOut"]).Begin();
        }

        // Fade Out Complete
        private void FadeOutAnimation_Completed(object sender, EventArgs e)
        {
            UpdateStatus();
        }

        // Raised Startup Completetion event
        private void UpdateStatus()
        {
            if (OnStartUpComplete == null) return;
            JobArgs args = new JobArgs(MainDetails);
            OnStartUpComplete(this, args);
        }
    }
}