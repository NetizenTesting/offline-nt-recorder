﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Interaction logic for surveyBuilderControl.xaml
    /// </summary>
    public partial class SurveyBuilderControl : UserControl
    {
        public delegate void surveyBuildCompletedHandler(object sender, JobArgs e);
        public event surveyBuildCompletedHandler OnSurveyBuildComplete;

        JobDetails MainDetails;

        List<TextBox> allSurveyQuestions = new List<TextBox>();

        public SurveyBuilderControl(JobDetails details)
        {
            InitializeComponent();

            MainDetails = details;
            allSurveyQuestions.Add(AnswerBox1);
            allSurveyQuestions.Add(AnswerBox2);
            allSurveyQuestions.Add(AnswerBox3);
            allSurveyQuestions.Add(AnswerBox4);

            if (MainDetails.EditProfile)
            {
                AnswerBox1.Text = MainDetails.SurveyQuestions[0];
                AnswerBox2.Text = MainDetails.SurveyQuestions[1];
                AnswerBox3.Text = MainDetails.SurveyQuestions[2];
                AnswerBox4.Text = MainDetails.SurveyQuestions[3];
            }
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            if (AnswerBox1.Text.Length == 0 || AnswerBox2.Text.Length == 0 || AnswerBox3.Text.Length == 0 || AnswerBox4.Text.Length == 0)
            {
                MsgBox msgBox = new MsgBox("Submit Error", "All question must be included.", MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();
            }
            else
            {
                using (StreamWriter addSurvey = new StreamWriter(MainDetails.TestProfile, true))
                {
                    addSurvey.Write("survey=");

                    for (int i = 0; i < allSurveyQuestions.Count; i++)
                    {
                        addSurvey.Write(allSurveyQuestions[i].Text.Replace("\r\n", @"\line"));

                        if (i != (allSurveyQuestions.Count - 1))
                        {
                            addSurvey.Write("<-Next->");
                        }
                    }
                }

                ((Storyboard)this.Resources["FadeOut"]).Begin();
                OnSurveyBuildComplete?.Invoke(this, new JobArgs(MainDetails));
            }
        }

        private void SurveyBuilder_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }
    }
}
