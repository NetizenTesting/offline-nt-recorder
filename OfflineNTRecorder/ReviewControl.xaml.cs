﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace OfflineNTRecorder
{

    public class ReviewEventArgs : EventArgs
    {
        public bool Proceed { get; private set; }

        public ReviewEventArgs(bool proceed)
        {
            Proceed = proceed;
        }
    }

	/// <summary>
	/// Interaction logic for ReviewControl.xaml
	/// </summary>
	public partial class ReviewControl : UserControl
	{
        bool result = false;

        public delegate void ReviewCompletedHandler(object sender, ReviewEventArgs e);
        public event ReviewCompletedHandler OnReviewComplete;
        Timer timer = new Timer(100);
        delegate void UpdateUI();

        private void reviewDone(bool audioDevice)
        {
            if (OnReviewComplete == null) return;
            ReviewEventArgs args = new ReviewEventArgs(audioDevice);
            OnReviewComplete(this, args);
        }

		public ReviewControl(string MediaLocation)
		{
			this.InitializeComponent();
            MediaElementReplayer.LoadedBehavior = MediaState.Manual;
            MediaElementReplayer.Source = new Uri(MediaLocation);
            MediaElementReplayer.MediaEnded += MediaElementReplayer_MediaEnded;
            timer.Elapsed += timer_Elapsed;
		}

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUI(UpdatePlayerControl), null);
        }

        void UpdatePlayerControl()
        {
            TimeSpan ts = MediaElementReplayer.Position;
            ReplayerTime.Content = ts.Minutes.ToString("D2") + ":" + ts.Seconds.ToString("D2");
        }

        void MediaElementReplayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, 0);
            MediaElementReplayer.Position = ts;
            MediaElementReplayer.Play();
        }

        private void PlayerButtonRedo_Click(object sender, RoutedEventArgs e)
        {
            MsgBox msgBox = new MsgBox("Review Confirmation", "Are you sure you want to redo the test?", MsgBox.MsgBoxType.YesNo);
            msgBox.ShowDialog();
            if (msgBox.Result)
            {
                result = false;
                this.Hide();
            }
        }

        private void PlayerButtonProceed_Click(object sender, RoutedEventArgs e)
        {
            MsgBox msgBox = new MsgBox("Review Confirmation", "Are you sure you want to upload the video?", MsgBox.MsgBoxType.YesNo);
            msgBox.ShowDialog();
            if (msgBox.Result)
            {
                result = true;
                this.Hide();
            }
        }

        private void Hide()
        {
            timer.Stop();
            MediaElementReplayer.Stop();
            MediaElementReplayer.Close();
            ((Storyboard)this.Resources["FadeOut"]).Begin();
        }

        private void MediaElementReplayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            //PlayerSlider.Maximum = MediaElementReplayer.NaturalDuration.TimeSpan.TotalMilliseconds;
        }

        private void ReviewUserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }

        private void FadeOutAnimation_Completed(object sender, EventArgs e)
        {
            reviewDone(result);
        }

        private void FadeInAnimation_Completed(object sender, EventArgs e)
        {
            MediaElementReplayer.Play();
            timer.Start();
        }

        private void PlayerSlider_DragOver(object sender, DragEventArgs e)
        {
        }

        private void PlayerSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //int SliderValue = (int)PlayerSlider.Value;

            // Overloaded constructor takes the arguments days, hours, minutes, seconds, miniseconds. 
            // Create a TimeSpan with miliseconds equal to the slider value.
            //TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);
            //MediaElementReplayer.Position = ts;
        }
	}
}