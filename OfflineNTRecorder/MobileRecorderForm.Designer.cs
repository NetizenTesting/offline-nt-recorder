﻿namespace OfflineNTRecorder
{
    partial class MobileRecorderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MobileRecorderForm));
            this.VideoDeviceComboBox = new System.Windows.Forms.ComboBox();
            this.AudioDeviceComboBox = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.picLoadingAnimationGif = new System.Windows.Forms.PictureBox();
            this.picPreviousArrow = new System.Windows.Forms.PictureBox();
            this.picNextArrow = new System.Windows.Forms.PictureBox();
            this.labelTip = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelRecordingTimer = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.labelClose = new System.Windows.Forms.Label();
            this.picMainNextArrow = new System.Windows.Forms.PictureBox();
            this.labelMainButton = new System.Windows.Forms.Label();
            this.labelMainHeader = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.picMouseDrag = new System.Windows.Forms.PictureBox();
            this.labelPrevious = new System.Windows.Forms.Label();
            this.labelNext = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.axWindowsMediaPlayer1 = new AxWMPLib.AxWindowsMediaPlayer();
            this.labelCountDown = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoadingAnimationGif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPreviousArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNextArrow)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMainNextArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMouseDrag)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).BeginInit();
            this.SuspendLayout();
            // 
            // VideoDeviceComboBox
            // 
            this.VideoDeviceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.VideoDeviceComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VideoDeviceComboBox.FormattingEnabled = true;
            this.VideoDeviceComboBox.ItemHeight = 20;
            this.VideoDeviceComboBox.Location = new System.Drawing.Point(30, 427);
            this.VideoDeviceComboBox.Name = "VideoDeviceComboBox";
            this.VideoDeviceComboBox.Size = new System.Drawing.Size(577, 28);
            this.VideoDeviceComboBox.TabIndex = 5;
            // 
            // AudioDeviceComboBox
            // 
            this.AudioDeviceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AudioDeviceComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AudioDeviceComboBox.FormattingEnabled = true;
            this.AudioDeviceComboBox.Location = new System.Drawing.Point(30, 472);
            this.AudioDeviceComboBox.Name = "AudioDeviceComboBox";
            this.AudioDeviceComboBox.Size = new System.Drawing.Size(577, 28);
            this.AudioDeviceComboBox.TabIndex = 6;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.picLoadingAnimationGif);
            this.panel2.Controls.Add(this.picPreviousArrow);
            this.panel2.Controls.Add(this.picNextArrow);
            this.panel2.Controls.Add(this.labelTip);
            this.panel2.Controls.Add(this.VideoDeviceComboBox);
            this.panel2.Controls.Add(this.AudioDeviceComboBox);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.labelPrevious);
            this.panel2.Controls.Add(this.labelNext);
            this.panel2.Controls.Add(this.richTextBox2);
            this.panel2.Location = new System.Drawing.Point(656, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(629, 546);
            this.panel2.TabIndex = 11;
            // 
            // pictureBox7
            // 
            this.picLoadingAnimationGif.Image = global::OfflineNTRecorder.Properties.Resources.loader;
            this.picLoadingAnimationGif.Location = new System.Drawing.Point(282, 230);
            this.picLoadingAnimationGif.Name = "pictureBox7";
            this.picLoadingAnimationGif.Size = new System.Drawing.Size(80, 80);
            this.picLoadingAnimationGif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoadingAnimationGif.TabIndex = 12;
            this.picLoadingAnimationGif.TabStop = false;
            this.picLoadingAnimationGif.Visible = false;
            // 
            // pictureBox4
            // 
            this.picPreviousArrow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(93)))), ((int)(((byte)(93)))));
            this.picPreviousArrow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picPreviousArrow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picPreviousArrow.Image = global::OfflineNTRecorder.Properties.Resources.grey_arrow;
            this.picPreviousArrow.Location = new System.Drawing.Point(7, 513);
            this.picPreviousArrow.Name = "pictureBox4";
            this.picPreviousArrow.Size = new System.Drawing.Size(10, 17);
            this.picPreviousArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picPreviousArrow.TabIndex = 16;
            this.picPreviousArrow.TabStop = false;
            this.picPreviousArrow.Visible = false;
            this.picPreviousArrow.Click += new System.EventHandler(this.PicPreviousArrow_Click);
            // 
            // pictureBox5
            // 
            this.picNextArrow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(93)))), ((int)(((byte)(93)))));
            this.picNextArrow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picNextArrow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picNextArrow.Image = global::OfflineNTRecorder.Properties.Resources.light_blue_arrow;
            this.picNextArrow.Location = new System.Drawing.Point(597, 512);
            this.picNextArrow.Name = "pictureBox5";
            this.picNextArrow.Size = new System.Drawing.Size(15, 22);
            this.picNextArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picNextArrow.TabIndex = 19;
            this.picNextArrow.TabStop = false;
            this.picNextArrow.Visible = false;
            this.picNextArrow.Click += new System.EventHandler(this.PicNextArrow_Click);
            // 
            // label11
            // 
            this.labelTip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelTip.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTip.ForeColor = System.Drawing.Color.White;
            this.labelTip.Location = new System.Drawing.Point(121, 500);
            this.labelTip.Name = "label11";
            this.labelTip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelTip.Size = new System.Drawing.Size(400, 44);
            this.labelTip.TabIndex = 18;
            this.labelTip.Text = "Tip: Always speak your thoughts out loud";
            this.labelTip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTip.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(204)))), ((int)(((byte)(203)))));
            this.panel3.Controls.Add(this.pictureBox6);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.labelClose);
            this.panel3.Controls.Add(this.picMainNextArrow);
            this.panel3.Controls.Add(this.labelMainButton);
            this.panel3.Controls.Add(this.labelMainHeader);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.picMouseDrag);
            this.panel3.Controls.Add(this.labelRecordingTimer);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(628, 62);
            this.panel3.TabIndex = 0;
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel3_MouseDown);
            // 
            // label12
            // 
            this.labelRecordingTimer.BackColor = System.Drawing.Color.Transparent;
            this.labelRecordingTimer.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.labelRecordingTimer.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecordingTimer.ForeColor = System.Drawing.Color.Black;
            this.labelRecordingTimer.Location = new System.Drawing.Point(444, 3);
            this.labelRecordingTimer.Name = "label12";
            this.labelRecordingTimer.Size = new System.Drawing.Size(113, 55);
            this.labelRecordingTimer.TabIndex = 17;
            this.labelRecordingTimer.Text = "--:--";
            this.labelRecordingTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelRecordingTimer.Visible = false;
            this.labelRecordingTimer.Click += new System.EventHandler(this.Label12_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox6.Image = global::OfflineNTRecorder.Properties.Resources.play_btn;
            this.pictureBox6.Location = new System.Drawing.Point(423, 10);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(42, 45);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox6.TabIndex = 16;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Visible = false;
            this.pictureBox6.Click += new System.EventHandler(this.PictureBox6_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(93)))), ((int)(((byte)(93)))));
            this.label8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label8.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(587, -1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 20);
            this.label8.TabIndex = 15;
            this.label8.Text = "?";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Visible = false;
            // 
            // label7
            // 
            this.labelClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(93)))), ((int)(((byte)(93)))));
            this.labelClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelClose.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClose.ForeColor = System.Drawing.Color.White;
            this.labelClose.Location = new System.Drawing.Point(607, -1);
            this.labelClose.Name = "label7";
            this.labelClose.Size = new System.Drawing.Size(20, 20);
            this.labelClose.TabIndex = 14;
            this.labelClose.Text = "X";
            this.labelClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelClose.Click += new System.EventHandler(this.LabelClose_Click);
            // 
            // pictureBox3
            // 
            this.picMainNextArrow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picMainNextArrow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picMainNextArrow.Image = global::OfflineNTRecorder.Properties.Resources.blue_arrow;
            this.picMainNextArrow.Location = new System.Drawing.Point(527, 7);
            this.picMainNextArrow.Name = "pictureBox3";
            this.picMainNextArrow.Size = new System.Drawing.Size(26, 48);
            this.picMainNextArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picMainNextArrow.TabIndex = 13;
            this.picMainNextArrow.TabStop = false;
            this.picMainNextArrow.Click += new System.EventHandler(this.PicMainNexArrow_Click);
            // 
            // label6
            // 
            this.labelMainButton.BackColor = System.Drawing.Color.Transparent;
            this.labelMainButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelMainButton.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMainButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(169)))), ((int)(((byte)(225)))));
            this.labelMainButton.Location = new System.Drawing.Point(438, 7);
            this.labelMainButton.Name = "label6";
            this.labelMainButton.Size = new System.Drawing.Size(87, 48);
            this.labelMainButton.TabIndex = 3;
            this.labelMainButton.Text = "Next";
            this.labelMainButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelMainButton.Click += new System.EventHandler(this.LabelMainButton_Click);
            // 
            // label4
            // 
            this.labelMainHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(93)))), ((int)(((byte)(93)))));
            this.labelMainHeader.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMainHeader.ForeColor = System.Drawing.Color.White;
            this.labelMainHeader.Location = new System.Drawing.Point(234, -1);
            this.labelMainHeader.Name = "label4";
            this.labelMainHeader.Size = new System.Drawing.Size(176, 62);
            this.labelMainHeader.TabIndex = 2;
            this.labelMainHeader.Text = "Test Setup";
            this.labelMainHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelMainHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label4_MouseDown);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::OfflineNTRecorder.Properties.Resources.netizen_logo;
            this.pictureBox2.Location = new System.Drawing.Point(69, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(110, 40);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox2_MouseDown);
            // 
            // pictureBox1
            // 
            this.picMouseDrag.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picMouseDrag.Image = global::OfflineNTRecorder.Properties.Resources.dots;
            this.picMouseDrag.Location = new System.Drawing.Point(3, 3);
            this.picMouseDrag.Name = "pictureBox1";
            this.picMouseDrag.Size = new System.Drawing.Size(23, 27);
            this.picMouseDrag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picMouseDrag.TabIndex = 0;
            this.picMouseDrag.TabStop = false;
            this.picMouseDrag.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureMouseDrag_MouseDown);
            // 
            // label9
            // 
            this.labelPrevious.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(93)))), ((int)(((byte)(93)))));
            this.labelPrevious.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelPrevious.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrevious.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.labelPrevious.Location = new System.Drawing.Point(0, 500);
            this.labelPrevious.Name = "label9";
            this.labelPrevious.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.labelPrevious.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelPrevious.Size = new System.Drawing.Size(124, 44);
            this.labelPrevious.TabIndex = 16;
            this.labelPrevious.Text = "Previous";
            this.labelPrevious.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPrevious.Visible = false;
            this.labelPrevious.Click += new System.EventHandler(this.LabelPrevious_Click);
            // 
            // label10
            // 
            this.labelNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(93)))), ((int)(((byte)(93)))));
            this.labelNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelNext.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNext.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(215)))), ((int)(((byte)(238)))));
            this.labelNext.Location = new System.Drawing.Point(519, 500);
            this.labelNext.Name = "label10";
            this.labelNext.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.labelNext.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelNext.Size = new System.Drawing.Size(108, 45);
            this.labelNext.TabIndex = 17;
            this.labelNext.Text = "Next";
            this.labelNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelNext.Visible = false;
            this.labelNext.Click += new System.EventHandler(this.LabelNext_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.Color.White;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(3, 62);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBox2.Size = new System.Drawing.Size(621, 435);
            this.richTextBox2.TabIndex = 1;
            this.richTextBox2.Text = "";
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Interval = 34000;
            this.timer3.Tick += new System.EventHandler(this.Timer3_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImage = global::OfflineNTRecorder.Properties.Resources.screen;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.axWindowsMediaPlayer1);
            this.panel1.Controls.Add(this.labelCountDown);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(656, 546);
            this.panel1.TabIndex = 4;
            // 
            // axWindowsMediaPlayer1
            // 
            this.axWindowsMediaPlayer1.Enabled = true;
            this.axWindowsMediaPlayer1.Location = new System.Drawing.Point(0, 0);
            this.axWindowsMediaPlayer1.Name = "axWindowsMediaPlayer1";
            this.axWindowsMediaPlayer1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer1.OcxState")));
            this.axWindowsMediaPlayer1.Size = new System.Drawing.Size(658, 546);
            this.axWindowsMediaPlayer1.TabIndex = 13;
            this.axWindowsMediaPlayer1.Visible = false;
            // 
            // labelCountDown
            // 
            this.labelCountDown.AutoSize = true;
            this.labelCountDown.BackColor = System.Drawing.Color.Black;
            this.labelCountDown.Font = new System.Drawing.Font("Arial", 130F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCountDown.ForeColor = System.Drawing.Color.Lime;
            this.labelCountDown.Location = new System.Drawing.Point(235, 162);
            this.labelCountDown.Name = "labelCountDown";
            this.labelCountDown.Size = new System.Drawing.Size(180, 198);
            this.labelCountDown.TabIndex = 0;
            this.labelCountDown.Text = "0";
            this.labelCountDown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCountDown.Visible = false;
            // 
            // MobileRecorderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.ClientSize = new System.Drawing.Size(1289, 556);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MobileRecorderForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mobile Recorder";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.DarkRed;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MobileRecorderForm_FormClosed);
            this.Load += new System.EventHandler(this.MobileRecorderForm_Load);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLoadingAnimationGif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPreviousArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNextArrow)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMainNextArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMouseDrag)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox VideoDeviceComboBox;
        private System.Windows.Forms.ComboBox AudioDeviceComboBox;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox picMouseDrag;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labelMainButton;
        private System.Windows.Forms.Label labelMainHeader;
        private System.Windows.Forms.PictureBox picMainNextArrow;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelClose;
        private System.Windows.Forms.Label labelCountDown;
        private System.Windows.Forms.Timer timer2;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer1;
        private System.Windows.Forms.Label labelTip;
        private System.Windows.Forms.Label labelNext;
        private System.Windows.Forms.Label labelPrevious;
        private System.Windows.Forms.PictureBox picPreviousArrow;
        private System.Windows.Forms.PictureBox picNextArrow;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label labelRecordingTimer;
        private System.Windows.Forms.PictureBox picLoadingAnimationGif;
    }
}