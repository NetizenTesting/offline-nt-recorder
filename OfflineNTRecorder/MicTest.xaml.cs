﻿using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.ScreenCapture;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Interaction logic for MicTest.xaml
    /// </summary>
    /// 

    public class MicTestEventArgs : EventArgs
    {
        public EncoderDevice AudioDevice { get; private set; }

        public MicTestEventArgs(EncoderDevice Device)
        {
            AudioDevice = Device;
        }
    }

    public partial class MicTest : UserControl
    {
        // Maybe can combine with the ones in NetizenTesting and Logger to clean up codes.
        void DeleteFileIfExists(string file)
        {
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        BackgroundWorker WorkerRecordingStart = new BackgroundWorker();
        BackgroundWorker WorkerRecordingDone = new BackgroundWorker();

        Collection<EncoderDevice> audioSources; // Expression Encoder
        string OutputfileName;
        string audioDeviceName;

        ScreenCaptureJob screenCapture; // Expression Encoder

        Timer timer = new Timer(100);
        delegate void UpdateUIwithString(string info);
        delegate void UpdateUI();
        public delegate void MicTestCompletedHandler(object sender, MicTestEventArgs e);
        public event MicTestCompletedHandler OnMicTestComplete;
        //LiveJob Job;
        //LiveDeviceSource Source;

        public MicTest(string MicTestOutputFile)
        {
            this.InitializeComponent();

            this.OutputfileName = MicTestOutputFile;

            audioSources = EncoderDevices.FindDevices(EncoderDeviceType.Audio);
            foreach (EncoderDevice device in audioSources)
            {
                if (device.Category == EncoderDeviceCategory.Capture)
                {
                    AudioDevicesComboBox.Items.Add(device.Name);
                }
            }
            AudioDevicesComboBox.SelectedIndex = 0;

            timer.Elapsed += timer_Elapsed;
            WorkerRecordingStart.DoWork += WorkerRecordingStart_DoWork;
            WorkerRecordingDone.DoWork += WorkerRecordingDone_DoWork;
            WorkerRecordingDone.RunWorkerCompleted += WorkerRecordingDone_RunWorkerCompleted;
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            RecordingProgressBar.Value = 0;
            RecordingProgressBar.Maximum = 50;
            ButtonStart.IsEnabled = false;
            AudioDevicesComboBox.IsEnabled = false;
            ButtonClose.IsEnabled = false;

            WorkerRecordingStart.RunWorkerAsync(AudioDevicesComboBox.SelectedIndex);
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            MsgBox msgBox = new MsgBox("Exit Confirmation", "Are you sure you want to quit the application?", MsgBox.MsgBoxType.YesNo);
            msgBox.ShowDialog();
            if (msgBox.Result)
            {
                Window.GetWindow(this).Close();
            }
        }

        void WorkerRecordingStart_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUIwithString(TextUpdate), "Preparing Microphone Test");

            DeleteFileIfExists(OutputfileName);

            int i = (int)e.Argument;

            // Expression Encoder start recording
            screenCapture = new ScreenCaptureJob();
            screenCapture.ShowCountdown = false;
            screenCapture.CaptureFollowCursor = false;
            screenCapture.OutputScreenCaptureFileName = OutputfileName;
            screenCapture.CaptureMouseCursor = true;
            screenCapture.AddAudioDeviceSource(audioSources[i]);
            screenCapture.Start();

            this.Dispatcher.BeginInvoke(new UpdateUIwithString(TextUpdate), "Microphone Test: Please Count Quickly");

            //Job = new LiveJob();
            //Source = Job.AddDeviceSource(null, audioSources[i]);
            //Job.ActivateSource(Source);
            //Job.ApplyPreset(LivePresets.VC1256kDSL16x9);

            //Job.PublishFormats.Clear();
            //Job.PublishFormats.Add(new FileArchivePublishFormat(OutputfileName));
            //Job.StartEncoding();

            timer.Start();

            audioDeviceName = audioSources[i].Name;
        }

        void TextUpdate(string text)
        {
            MainText.Text = text;
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new UpdateUI(UpdateProgress), null);
        }

        void UpdateProgress()
        {
            RecordingProgressBar.Value += 1;
            if (RecordingProgressBar.Value == 50)
            {
                timer.Stop();
                WorkerRecordingDone.RunWorkerAsync();
            }
        }

        void WorkerRecordingDone_DoWork(object sender, DoWorkEventArgs e)
        {
            screenCapture.Stop();
        }

        void WorkerRecordingDone_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RecordingProgressBar.Value = 0;
            ButtonStart.IsEnabled = true;
            AudioDevicesComboBox.IsEnabled = true;
            ButtonClose.IsEnabled = true;

            this.Dispatcher.BeginInvoke(new UpdateUIwithString(TextUpdate), "Preparing Recording Replay");

            MediaElement m = new MediaElement();
            Uri myuri = new Uri(OutputfileName);
            m.Source = myuri;
            m.UnloadedBehavior = MediaState.Manual;
            m.Play();

            this.Dispatcher.BeginInvoke(new UpdateUIwithString(TextUpdate), "Recording Replay");

            MsgBox msgBox = new MsgBox("Mic Test", "Can you hear?", MsgBox.MsgBoxType.MicTest);
            msgBox.ShowDialog();
            this.Dispatcher.BeginInvoke(new UpdateUIwithString(TextUpdate), "Microphone Test");
            if (msgBox.Result)
            {
                this.Hide();
            }
            else if (msgBox.Replay)
            {
                msgBox = new MsgBox("Mic Test", "Can you hear?", MsgBox.MsgBoxType.MicTest);
                msgBox.ShowDialog();
                bool micReplay = msgBox.Replay;
                while (micReplay)
                {
                    m.Stop(); 
                    TimeSpan ts = new TimeSpan(0, 0, 0, 0, 0);
                    m.Position = ts;
                    m.Play();
                    msgBox = new MsgBox("Mic Test", "Can you hear?", MsgBox.MsgBoxType.MicTest);
                    msgBox.ShowDialog();
                    micReplay = msgBox.Replay;

                }
                if (msgBox.Result)
                {
                    this.Hide();
                }
            }
            m.Stop();
            m.Close();
        }

        private void Hide()
        {
            ((Storyboard)this.Resources["FadeOut"]).Begin();
        }

        private void FadeOutAnimation_Completed(object sender, EventArgs e)
        {
            if (audioSources.Count == 0)
            {
                MsgBox msgBox = new MsgBox("Exit Error", "No audio Capture device found.", MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();
                Window.GetWindow(this).Close();
            }
            foreach (EncoderDevice device in audioSources)
            {
                if (device.Name == audioDeviceName)
                    micTestDone(device);
            }
        }

        private void micTestDone(EncoderDevice audioDevice)
        {
            if (OnMicTestComplete == null) return;
            MicTestEventArgs args = new MicTestEventArgs(audioDevice);
            OnMicTestComplete(this, args);
        }
    }
}