﻿using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.Live;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NetizenTestingDesktop
{
	/// <summary>
	/// Interaction logic for MobileRecorderControl.xaml
	/// </summary>
	public partial class MobileRecorderControl : UserControl
    {
        Collection<EncoderDevice> audioSources;
        Collection<EncoderDevice> videoSources;
        private enum RecordingState
        {
            Preview,
            Scenario,
            Recording,
            Paused,
            Completed
        }
        private string scenario, fileLocation;
        private List<string> instructions;
        private RecordingState currentState = RecordingState.Preview;
        private int currentInstruction = 0;

        private void SetRichTextBox(string text, bool type)
        {
            if (type)
            {
                RichTextBoxInfo.Document.Blocks.Clear();
                RichTextBoxInfo.Document.Blocks.Add(new Paragraph(new Run("Task " + (currentInstruction + 1).ToString() + ": " + text)));
                RichTextBoxInfo.CaretPosition = RichTextBoxInfo.CaretPosition.DocumentStart;
            }
            else
            {
                RichTextBoxInfo.Document.Blocks.Clear();
                RichTextBoxInfo.Document.Blocks.Add(new Paragraph(new Run("Scenario: " + text)));
                RichTextBoxInfo.CaretPosition = RichTextBoxInfo.CaretPosition.DocumentStart;
            }
        }

        public MobileRecorderControl(string OutputFile)
		{
			this.InitializeComponent();
            fileLocation = OutputFile;
            audioSources = EncoderDevices.FindDevices(EncoderDeviceType.Audio);
            foreach (EncoderDevice device in audioSources)
            {
                if (device.Category == EncoderDeviceCategory.Capture)
                {
                    AudioDevicesComboBox.Items.Add(device.Name);
                }
            }
            AudioDevicesComboBox.SelectedIndex = 0;
            videoSources = EncoderDevices.FindDevices(EncoderDeviceType.Video);
            foreach (EncoderDevice device in videoSources)
            {
                if (device.Category == EncoderDeviceCategory.Capture)
                {
                    VideoDevicesComboBox.Items.Add(device.Name);
                }
            }
            VideoDevicesComboBox.SelectedIndex = 0;
            BackButton.IsEnabled = false;
            NextButton.IsEnabled = false;
		}

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                Window.GetWindow(this).DragMove();
        }


        private void FadeOutAnimation_Completed(object sender, EventArgs e)
        {

        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            MsgBox msgBox = new MsgBox("Exit Confirmation", "Are you sure you want to quit the application?", MsgBox.MsgBoxType.YesNo);
            msgBox.ShowDialog();
            if (msgBox.Result)
            {
                Window.GetWindow(this).Close();
            }
        }

        LiveJob job;
        LiveDeviceSource deviceSource;
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            switch (currentState)
            {
                case RecordingState.Preview:
                    job = new LiveJob();
                    deviceSource = job.AddDeviceSource(videoSources[VideoDevicesComboBox.SelectedIndex], audioSources[AudioDevicesComboBox.SelectedIndex]);
                    deviceSource.PreviewWindow = new PreviewWindow(new HandleRef(panel1, panel1.Handle));
                    job.ActivateSource(deviceSource);
                    
                    FileArchivePublishFormat fileOut = new FileArchivePublishFormat();
                    job.ApplyPreset(LivePresets.VC1HighSpeedBroadband4x3);
                    fileOut.OutputFileName = fileLocation;
                    job.PublishFormats.Add(fileOut);
                    break;
            }
        }
	}
}