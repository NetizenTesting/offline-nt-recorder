﻿using Microsoft.Expression.Encoder.Devices;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace OfflineNTRecorder
{
    /// <summary>
    /// Netizen Testing is the main control for Offline NT Recorder
    /// </summary>
    /// 
    /// ===================================================================================================================================
    /// Revision History for Offline NT Recorder
    /// 
    /// Revision#       Description                                                     Initials        Date
    /// -----------------------------------------------------------------------------------------------------------------------------------
    /// 1.0.2.1         Offline NT Recorder is based off NT Recorder                    Mike            11-May-2017
    ///                 with all online items removed
    /// 1.0.2.2         Added a Task Builder                                            Mike            22-May-2017
    ///
    public partial class NetizenTestingStartup : Window
    {
        enum Status
        {
            Error = -1,
            CheckEncoder,
            Start,
            Main,
            AddProfile,
            AddSurvey,
            StartRecording,
            RecordingCompleted
        }

        enum WindowLocation
        {
            Center,
            TopCenter
        }

        // Folder and Files. 
        // User directory is used for manual upload if needed
        static string downloadLocation;
        static string screenCaptureFile;
        static string micTestFile;
        static string uploadFile = "Upload.mp4";

        ErrorLogger errorLogger = new ErrorLogger("MainLog.log");

        // Controls
        StartUp StartUpControl;
        ScreenCapture ScreenCaptureControl;
        ReviewControl VideoReviewer;
        VideoEncoderControl encoderControl = null;
        MainControl mainControl = null;
        InstallControl installControl = null;
        SurveyControl surveyControl;
        EncoderDevice audioDevice;
        TaskBuilderControl taskBuildControl;
        SurveyBuilderControl surveyBuildControl;

        // Job Related Variables
        string url = "<-Disable->";
        string scenario = "Test";
        List<string> tasks = new List<string>();

        JobDetails mainDetails = new JobDetails();

        public NetizenTestingStartup()
        {
            this.InitializeComponent();

            screenCaptureFile = mainDetails.TestFilesFolder + "ScreenCapture.wmv";
            micTestFile = mainDetails.TestFilesFolder + "MicTest.wmv";
            downloadLocation = mainDetails.MainFolder + "encoder.zip";

            CreateDirectoryIfNotExist(mainDetails.MainFolder);

            ControlSwitcher(Status.CheckEncoder);
        }

        // File and Folder Management
        void CreateDirectoryIfNotExist(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        void DeleteFileIfExists(string file)
        {
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        void DeleteDirectoryIfExist(string directory)
        {
            if (Directory.Exists(directory))
            {
                Directory.Delete(directory, true);
            }
        }

        // Load in caller control at the correct location
        private void LoadControl(UserControl Control, WindowLocation Location)
        {
            // Match the loading control's size
            this.Width = Control.Width;
            this.Height = Control.Height;

            // Load in caller control
            if (ContentGrid.Children.Count > 0)
            {
                ContentGrid.Children.RemoveAt(0);
            }
            ContentGrid.Children.Add(Control);

            // Get the center point of the display area
            double width = SystemParameters.WorkArea.Width / 2;
            double height = SystemParameters.WorkArea.Height / 2;

            // Set the control to the correct location
            switch (Location)
            {
                case WindowLocation.Center:
                    this.Left = width - Control.Width / 2;
                    this.Top = height - Control.Height / 2;
                    break;
                case WindowLocation.TopCenter:
                    this.Left = width - Control.Width / 2;
                    this.Top = 0;
                    break;
            }
        }

        // Switches between control depending on Status
        private void ControlSwitcher(Status status)
        {
            switch (status)
            {
                case Status.Error:
                    MsgBox msgBox = new MsgBox("Error", "Oops! We are sorry, something went wrong. Please contact support.", MsgBox.MsgBoxType.Ok);
                    msgBox.ShowDialog();
                    if (msgBox.Replay)
                    {
                        this.Close();
                    }

                    break;
                case Status.CheckEncoder:
                    errorLogger.AppendLog("Checking Expression Encoder Version");
                    CheckEncoder();

                    break;
                case Status.Start:
                    errorLogger.AppendLog("Starting...");

                    StartUpControl = new StartUp(mainDetails);
                    StartUpControl.OnStartUpComplete += StartUpControl_OnStartUpComplete;
                    LoadControl(StartUpControl, WindowLocation.Center);

                    break;
                case Status.Main:
                    errorLogger.AppendLog("Loading Main Control");

                    mainControl = new MainControl(mainDetails);
                    mainControl.OnMainComplete += MainControl_OnLoginComplete;
                    LoadControl(mainControl, WindowLocation.Center);

                    break;
                case Status.AddProfile:
                    errorLogger.AppendLog("Loading Task Builder");

                    taskBuildControl = new TaskBuilderControl(mainDetails);
                    taskBuildControl.OnTaskBuildComplete += TaskBuildControl_OnTaskBuildComplete;
                    LoadControl(taskBuildControl, WindowLocation.Center);

                    break;
                case Status.AddSurvey:
                    errorLogger.AppendLog("Loading Survey Builder");

                    surveyBuildControl = new SurveyBuilderControl(mainDetails);
                    surveyBuildControl.OnSurveyBuildComplete += SurveyBuildControl_OnSurveyBuildComplete;
                    LoadControl(surveyBuildControl, WindowLocation.Center);

                    break;
                case Status.StartRecording:
                    MobileRecorderForm form = new MobileRecorderForm(mainDetails);
                    form.ShowDialog();

                    if (form.testSuccess)
                    {
                        ControlSwitcher(Status.RecordingCompleted);
                    }
                    else
                    {
                        errorLogger.AppendLog("Mobile Recorder Test was unsuccessful");
                        this.Close();
                    }

                    break;
                case Status.RecordingCompleted:
                    if (mainDetails.AddSurvey)
                    {
                        errorLogger.AppendLog("Loading survey");

                        surveyControl = new SurveyControl(mainDetails);
                        surveyControl.OnSurveyComplete += SurveyControl_OnSurveyComplete;
                        LoadControl(surveyControl, WindowLocation.Center);
                    }
                    else
                    {
                        ControlSwitcher(Status.Main);
                    }

                    break;
                default:
                    return;
            }
        }

        // Check if Expression Encoder is installed. If not then start installation
        private void CheckEncoder()
        {
            try
            {
                string encoderVersion = "";

                RegistryKey localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Default);
                localKey = localKey.OpenSubKey(@"SOFTWARE\Microsoft\Expression\Encoder\4.0");

                DeleteDirectoryIfExist(mainDetails.MainFolder + @"encoder\");
                DeleteFileIfExists(mainDetails.MainFolder + "encoder.zip");

                if (localKey != null)
                {
                    encoderVersion = localKey.GetValue("Version").ToString();
                }
                else
                {
                    errorLogger.AppendLog("Expression Encoder not installed");
                    MsgBox msgBox = new MsgBox("Installation Required", "Expression encoder not found. Expression Encoder will be downloaded and installed.", MsgBox.MsgBoxType.Ok);
                    msgBox.ShowDialog();

                    installControl = new InstallControl(downloadLocation, mainDetails.MainFolder);
                    LoadControl(installControl, WindowLocation.Center);

                    return;
                }

                if (encoderVersion != "4.0.4276.0")
                {
                    errorLogger.AppendLog("Expression Encoder - wrong version - " + encoderVersion);
                    MsgBox msgBox = new MsgBox("Installation Required", "Wrong version of expression encoder installed. Expression Encoder will updated.", MsgBox.MsgBoxType.Ok);
                    msgBox.ShowDialog();

                    installControl = new InstallControl(downloadLocation, mainDetails.MainFolder);
                    LoadControl(installControl, WindowLocation.Center);

                    return;
                }

                ControlSwitcher(Status.Start);
            }
            catch (Exception exception)
            {
                errorLogger.ExceptionLog(exception);
                ControlSwitcher(Status.Error);
            }
        }

        // Startup Complete
        private void StartUpControl_OnStartUpComplete(object sender, JobArgs e)
        {
            ControlSwitcher(Status.Main);
        }

        // Main Complete
        private void MainControl_OnLoginComplete(object sender, JobArgs e)
        {
            if (mainDetails.AddProfile)
            {
                if (mainDetails.EditProfile)
                {
                    if(!RetrieveJob())
                    {
                        ControlSwitcher(Status.Error);
                        return;
                    }
                }
                ControlSwitcher(Status.AddProfile);
            }
            else
            {
                if (RetrieveJob())
                {
                    ControlSwitcher(Status.StartRecording);
                }
                else
                {
                    ControlSwitcher(Status.Error);
                }
            }
        }

        // Task Build Complete
        private void TaskBuildControl_OnTaskBuildComplete(object sender, JobArgs e)
        {
            if (mainDetails.Error)
            {
                ControlSwitcher(Status.Error);
            }
            else if (mainDetails.AddSurvey)
            {
                ControlSwitcher(Status.AddSurvey);
            }
            else
            {
                ControlSwitcher(Status.Main);
            }
        }

        // Survey Build Complete
        private void SurveyBuildControl_OnSurveyBuildComplete(object sender, JobArgs e)
        {
            ControlSwitcher(Status.Main);
        }

        //Retrieve Job info from selected test profile
        private bool RetrieveJob()
        {
            NameValueCollection info = new NameValueCollection();
            string[] details = File.ReadAllLines(mainDetails.TestProfile);
            

            if (details.Length > 2)
            {
                foreach (string infoString in details)
                {
                    string tempString = "";
                    string[] infoDetails = infoString.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 1; i < infoDetails.Length; i++)
                    {
                        tempString += infoDetails[i];

                        if (i != (infoDetails.Length - 1))
                        {
                            tempString += "=";
                        }
                    }
                    info.Add(infoDetails[0], tempString);
                }
            }
            else
            {
                errorLogger.AppendLog("Details has less than 2 line. Check Test Profile");
                return false;
            }

            if (info.Count > 2)
            {
                if (info["scenario"] == null || info["task"] == null || info["survey"] == null)
                {
                    errorLogger.AppendLog("One of the profile item is null");
                    return false;
                }
                else
                {
                    if (info["url"] == null)
                    {
                        mainDetails.Url = "<-Disable->";
                    }
                    else
                    {
                        mainDetails.Url = info["url"];
                    }
                    mainDetails.Scenario = info["scenario"];
                    mainDetails.Tasks = new List<string>(info["task"].Split(new string[] { "<-Next->" }, StringSplitOptions.RemoveEmptyEntries));

                    if (info["survey"] == "<-Disable->")
                    {
                        mainDetails.AddSurvey = false;
                    }
                    else
                    {
                        mainDetails.AddSurvey = true;

                        info["survey"] = info["survey"].Replace(@"\line", "\r\n");
                        mainDetails.SurveyQuestions = new List<string>(info["survey"].Split(new string[] { "<-Next->" }, StringSplitOptions.RemoveEmptyEntries));
                    }
                    
                    return UpdateSettings();
                }
            }
            else
            {
                errorLogger.AppendLog("Info count is less than 2. Check Test Profile");
                return false;
            }
        }

        // Update the items in the settings file
        private bool UpdateSettings()
        {
            if (mainDetails.TestProfile != "")
            {
                string line = File.ReadAllText(mainDetails.SettingsFile);
                string[] lines = line.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

                if(lines[0] == "profile")
                {
                    if (lines[1] != Path.GetFileName(mainDetails.TestProfile))
                    {
                        File.WriteAllText(mainDetails.SettingsFile, "profile=" + Path.GetFileName(mainDetails.TestProfile));
                    }
                    return true;
                }
                else
                {
                    errorLogger.AppendLog("Missing profile settting");
                    return false;
                }
            }
            else
            {
                errorLogger.AppendLog("Test Profile is null");
                return false;
            }
        }

        // Desktop Mic Test Complete
        void MicTestControl_OnMicTestComplete(object sender, MicTestEventArgs e)
        {
            DeleteFileIfExists(screenCaptureFile);
            audioDevice = e.AudioDevice;
            ScreenCaptureControl = new ScreenCapture(scenario, tasks, screenCaptureFile, audioDevice, url);
            ScreenCaptureControl.OnScreenCaptureComplete += ScreenCaptureControl_OnScreenCaptureComplete;
            LoadControl(ScreenCaptureControl, WindowLocation.TopCenter);
            errorLogger.AppendLog("Starting screen capture");
        }

        // Desktop Screen Record Complete
        void ScreenCaptureControl_OnScreenCaptureComplete(object sender, EventArgs e)
        {
            VideoReviewer = new ReviewControl(screenCaptureFile);
            VideoReviewer.OnReviewComplete += VideoReviewer_OnReviewComplete;
            LoadControl(VideoReviewer, WindowLocation.Center);
            errorLogger.AppendLog("Starting video review");
        }

        // Desktop Video Review
        void VideoReviewer_OnReviewComplete(object sender, ReviewEventArgs e)
        {
            if (e.Proceed)
            {
                DeleteFileIfExists(uploadFile);
                surveyControl = new SurveyControl(mainDetails);
                surveyControl.OnSurveyComplete += SurveyControl_OnSurveyComplete;
                LoadControl(surveyControl, WindowLocation.Center);
                errorLogger.AppendLog("Starting survey");
            }
            else
            {
                DeleteFileIfExists(screenCaptureFile);
                ScreenCaptureControl = new ScreenCapture(scenario, tasks, screenCaptureFile, audioDevice, url);
                ScreenCaptureControl.OnScreenCaptureComplete += ScreenCaptureControl_OnScreenCaptureComplete;
                LoadControl(ScreenCaptureControl, WindowLocation.TopCenter);
                errorLogger.AppendLog("Restarting screen recorder");
            }
        }

        // Survey Complete
        void SurveyControl_OnSurveyComplete(object sender, JobArgs e)
        {
            if (encoderControl == null)
            {
                using (StreamWriter logFile = new StreamWriter(mainDetails.SurveyAnswerFile))
                {
                    for (int i = 0; i < mainDetails.SurveyAnswers.Count; i++)
                    {
                        logFile.WriteLine("[{0}]", mainDetails.SurveyQuestions[i]);
                        logFile.WriteLine("{0}", mainDetails.SurveyAnswers[i]);
                        logFile.WriteLine();
                    }
                }

                errorLogger.AppendLog(mainDetails.TesterName + " Test Completed");
                MsgBox msgBox = new MsgBox("Test completed", "Thank you for completing this test", MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();

                ControlSwitcher(Status.Main);                
            }
        }

        // Encoding Complete
        void EncoderControl_OnEncoderComplete(object sender, CompleteEventArgs e)
        {
            if (e.Success)
            {
                /*if (uploaderControl == null)
                {
                    uploaderControl = new VideoUploaderControl(uploadFile, uploadFileName);
                    uploaderControl.OnUploadComplete += uploaderControl_OnUploadComplete;
                    LoadControl(uploaderControl, WindowLocation.Center);
                    errorLogger.AppendLog("Starting upload");
                }*/
            }
            else
            {
                errorLogger.AppendLog("Encoder Failed");
                MsgBox msgBox = new MsgBox("Error", "Encoding failed. Retry by starting the program from browser.", MsgBox.MsgBoxType.Ok);
                msgBox.ShowDialog();
                this.Close();
            }
        }
    }
}